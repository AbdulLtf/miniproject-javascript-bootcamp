//2
//FILE UTAMA, TAPI UNTUK MENAMPILKAN INDEK.HTML
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App' // App ialah variabel, ./App ialah nama file nya, kebetulan sama.
import { BrowserRouter } from 'react-router-dom'

// import {BrowserRouter} from 'react-router-dom'

ReactDOM.render( // brarti merender smua file react dari sumber ke tujuan // knepa pkai browser router, karena digunakn switch
    <BrowserRouter> 
        <App /> 
    </BrowserRouter>,
    document.getElementById("app") // <<<<< destination, app ialah sumber // app huruf kecil ini ialah nama id pada index.html (di body) berbeda dgn App di atas

)