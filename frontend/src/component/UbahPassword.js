import React from "react";
import apiconfig from "../configs/api.configs.json";
import axios from "axios";
import { Link, Route, Switch } from "react-router-dom";

class UbahPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userrole: [],
      formdata: {
        password1: "",
        password2: ""
      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      formdata: tmp
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    alert(JSON.stringify(this.state.formdata));
    let pwd1 = this.state.formdata.password1;
    let pwd2 = this.state.formdata.password2;
    if (pwd1 === pwd2) {
      this.UbahPassword();
      localStorage.clear();
      this.props.history.push("/");
      alert("silahkan login kembali");
    } else {
      this.props.history.push("/ubahpassword");
      alert("pasword tidak sama");
    }
  }
  onSignOut() {
    localStorage.clear();
  }
  UbahPassword() {
    var abuid = localStorage.getItem(apiconfig.LS.USERNAME);
    var abpwd = this.state.formdata.password1;

    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.UBAHPWD,
      method: "PUT",
      headers: {
        Authorization: token
      },
      data: { abuid: abuid, abpwd: abpwd }
    };
    axios(option)
      .then(response => {
        if (response.data.code === 200) {
          let tmp = response.data.message;
          this.setState({
            userrole: tmp
          });
        } else {
          alert(response.data.message);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    let x = localStorage.getItem(apiconfig.LS.USERNAME);
    return (
      <div>
        <div style={{ height: "4em", backgroundColor: "#000099" }}>
          <Link
            className="nav-link"
            to=""
            onClick={this.onSignOut}
            style={{ paddingTop: "1.5%", color: "#ffffff" }}
          >
            Sign out
          </Link>
        </div>
        <div
          style={{
            margin: "3% 10% 0 10%",
            height: "5em",
            backgroundColor: "#000099",
            border: "1px solid",
            padding: "3px",
            boxShadow: "5px 10px #000099",
            borderRadius: "3px",
            color: "white"
          }}
        >
          <h1 style={{ marginLeft: "4%", marginTop: "1%" }}>HI,{x}</h1>
        </div>
        <div
          className="jumbotron "
          style={{
            margin: "0 10% 0 10%",
            backgroundColor: "white",
            border: "1px solid",
            padding: "3px",
            boxShadow: "5px 10px #000099",
            height: "40%"
          }}
        >
          <div className="card">
            <div
              className="card-body login-card-body"
              style={{
                width: "600px",
                marginLeft: "25%"
              }}
            >
              <form onSubmit={this.handleSubmit}>
                <p className="login-box-msg">GANTI & MASUKAN PASSWORD BARU </p>
                <div className="input-group mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Password Baru"
                    name="password1"
                    required=""
                    autoFocus=""
                    value={this.state.password1}
                    onChange={this.handleChange}
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                <div className="input-group mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Re-Password Baru"
                    name="password2"
                    required=""
                    value={this.state.password2}
                    onChange={this.handleChange}
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                <input
                  className="bg-primary"
                  type="submit"
                  value="Ganti Password"
                />
                <span
                  style={{
                    float: "right",
                    boxSizing: "border-box",
                    border: "1px solid #e3e3e3",
                    backgroundColor: "yellow",
                    height: "30px",
                    marginTop: "-5px",
                    borderRadius: "3px",
                    boxShadow: "5px #e3e3e3"
                  }}
                >
                  <a
                    type="button"
                    className="nav-link"
                    href="/dashboard2"
                    style={{ color: "red", marginTop: "-5px" }}
                  >
                    batal ubah
                  </a>
                </span>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default UbahPassword;
