import React from "react";
import { Link } from "react-router-dom";
import apiconfig from "../configs/api.configs.json";
import axios from "axios";

class Sidebar2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      usermenu: [],
    };
  }
  onSignOut() {
    localStorage.clear();
  }

  getListMenu() {
    var x = localStorage.getItem(apiconfig.LS.CHOICE);
    // alert(x)
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.USERMENU,
      method: "POST",
      headers: {
        Authorization: token,
      },
      data: { name: x },
    };
    axios(option)
      .then((response) => {
        if (response.data.code === 200) {
          let tmp = response.data.message;
          this.setState({
            usermenu: tmp,
          });
          this.props.history.push("/dashboard2");
        } else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getListMenu();
  }
  render() {
    return (
      <div
        style={{
          backgroundColor: "#6c757d",
          padding: "10px",
          borderRadius: "5px",
          height: "498px",
          width: "250px",
          overflow: "auto",
        }}
      >
        <div style={{ marginBottom: "-435px", width: "150px" }}>
          <div className="row">
            <div className="col-sm-4">
              <img
                src="dist/img/user1-128x128.jpg"
                alt="User Avatar"
                class="img-size-50 mr-3 img-circle"
              ></img>
            </div>
            <div className="col-sm-8">
              <h3
                style={{
                  marginTop: "10px",
                  marginLeft: "10px",
                  color: "white",
                }}
                class="dropdown-item-title"
              >
                Abdul Latief
              </h3>
            </div>
          </div>
        </div>

        {/* ----------------MENU DAN SUBMENU----------------- */}
        <div
          className="btn-group-vertical"
          style={{
            paddingRight: "30px",
            marginLeft: "-4px",
            height: "1000px",
            overflow: "auto",
          }}
        >
          <div>
            {this.state.usermenu.map((row) =>
              row.menu_level === 1 &&
              row.menu_parent === null &&
              row.menu_type === "navbar" ? (
                <div>
                  <hr style={{ width: "150px" }} />
                  <div
                    key={row.id}
                    className="btn-group"
                    role="group"
                    aria-label="Button group with nested dropdown"
                  >
                    <div
                      className="btn-group"
                      role="group"
                      style={{ width: "190px" }}
                    >
                      <button
                        id="btnGroupDrop1"
                        type="button"
                        className="btn btn-secondary dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        style={{ margin: "-12px 0 -12px 0" }}
                      >
                        {row.title}
                      </button>
                      <div
                        className="dropdown-menu"
                        aria-labelledby="btnGroupDrop1"
                        style={{ width: "95%" }}
                      >
                        {this.state.usermenu.map((rows) =>
                          rows.menu_level === 2 &&
                          rows.menu_parent === row.id &&
                          row.menu_type === "navbar" ? (
                            <Link
                              key={rows.id}
                              className="dropdown-item"
                              to={rows.menu_url}
                              name="choice"
                            >
                              <i className="far fa-circle nav-icon"></i>
                              <span style={{ marginLeft: "2px" }}>
                                {rows.title}
                              </span>
                            </Link>
                          ) : null
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              ) : null
            )}
            {/* ------------------------------------------------------ */}

            <hr style={{ width: "150px" }} />
            <button
              type="button"
              class="btn btn-secondary"
              style={{
                margin: "-12px 0 -12px 0",
                height: "35px",
                width: "190px",
              }}
            >
              <a
                className="nav-link"
                href="/"
                style={{ color: "white", marginTop: "-10px" }}
                onClick={this.onSignOut}
              >
                Sign Out
              </a>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Sidebar2;

{
  /* <div
  style={{
    backgroundColor: "#6c757d",
    padding: "10px",
    borderRadius: "5px",
    height: "498px",
    width: "250px",
    overflow: "auto"
  }}
>
  <div style={{ marginBottom: "-435px", width: "150px" }}>
    <div className="row">
      <div className="col-sm-4">
        <img
          src="dist/img/user1-128x128.jpg"
          alt="User Avatar"
          class="img-size-50 mr-3 img-circle"
        ></img>
      </div>
      <div className="col-sm-8">
        <h3
          style={{
            marginTop: "10px",
            marginLeft: "10px",
            color: "white"
          }}
          class="dropdown-item-title"
        >
          Abdul Latief
        </h3>
      </div>
    </div>
  </div>

  <div
    className="btn-group-vertical"
    style={{
      paddingRight: "30px",
      marginLeft: "-4px",
      height: "1000px",
      overflow: "auto"
    }}
  >
    <div>
      {this.state.usermenu.map(row =>
        row.menu_level === 1 &&
        row.menu_parent === null &&
        row.menu_type === "navbar" ? (
          <div>
            <hr style={{ width: "150px" }} />
            <div
              key={row.id}
              className="btn-group"
              role="group"
              aria-label="Button group with nested dropdown"
            >
              <div
                className="btn-group"
                role="group"
                style={{ width: "190px" }}
              >
                <button
                  id="btnGroupDrop1"
                  type="button"
                  className="btn btn-secondary dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style={{ margin: "-12px 0 -12px 0" }}
                >
                  {row.title}
                </button>
                <div
                  className="dropdown-menu"
                  aria-labelledby="btnGroupDrop1"
                  style={{ width: "95%" }}
                >
                  {this.state.usermenu.map(rows =>
                    rows.menu_level === 2 && rows.menu_parent === row.id ? (
                      <Link
                        key={rows.id}
                        className="dropdown-item"
                        to={rows.menu_url}
                        name="choice"
                      >
                        <i className="far fa-circle nav-icon"></i>
                        <span style={{ marginLeft: "2px" }}>{rows.title}</span>
                      </Link>
                    ) : null
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : null
      )}
      <hr style={{ width: "150px" }} />
      <button
        type="button"
        class="btn btn-secondary"
        style={{
          margin: "-12px 0 -12px 0",
          height: "35px",
          width: "190px"
        }}
      >
        <a
          className="nav-link"
          href="/"
          style={{ color: "white", marginTop: "-10px" }}
          onClick={this.onSignOut}
        >
          Sign Out
        </a>
      </button>
    </div>
  </div>
</div>; */
}
