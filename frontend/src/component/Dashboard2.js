import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import apiconfig from "../configs/api.configs.json";
import Header2 from "./Header2";
import Sidebar2 from "./Sidebar2";
import Menubar from "./Menubar";
import Home from "./content/home";
import Undangan from "./content/undangan/listundangan";
import Rencana from "./content/rencana/listrencana";

class Dashboard2 extends React.Component {
  render() {
    var x = localStorage.getItem(apiconfig.LS.CHOICE);
    return (
      <div style={{ backgroundColor: "grey", height: "675px" }}>
        <Header2 />
        <center>
          <h3
            style={{
              marginBottom: "2%",
              color: "#000099",
              marginTop: "-3%",
              fontWeight: "bold",
            }}
          >
            {x}
          </h3>
        </center>
        <div className="mt-1" style={{ margin: "2em" }}>
          <div className="row">
            <Router>
              <div className="col-lg-2">
                <Sidebar2 />
                {/* <Menubar /> */}
              </div>
              <div className="col-lg-10">
                <div
                  style={{
                    height: "500px",
                    width: "100%",
                    backgroundColor: "white",
                    overflow: "auto",
                    borderRadius: "5px",
                  }}
                >
                  <section className="content">
                    <Switch>
                      <Route path="/dashboard2">
                        <Home />
                      </Route>{" "}
                      <Route path="/undangan">
                        <Undangan />
                      </Route>
                      <Route path="/rencana">
                        <Rencana />
                      </Route>
                    </Switch>
                  </section>
                </div>
              </div>
            </Router>
          </div>
        </div>
        <div
          style={{
            marginTop: "-2%",
            marginBottom: "-200px",
            backgroundColor: "#000099",
            color: "white",
          }}
        >
          <center>
            <h5>© Batch 236 React JS</h5>
          </center>
        </div>
      </div>
    );
  }
}
export default Dashboard2;
