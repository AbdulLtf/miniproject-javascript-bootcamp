//4
import React from "react";
import API from "../helpers/API"; // API isinya method, tujuan nya ialah semua tproces ajax di situ/ untk memamnggil backendnya disitu
import config from "../configs/api.configs.json";

// SEMUA FUNGSI ATAU METHOD HARUS DIATAS RENDER
// APA YG DITAMPILKAN DALAM RETURN

class Login extends React.Component {
  constructor(props) {
    // TMPAT MENDEFENISIKAN SEMUA PARAMETER DAN METHOD YG DIPAKAI, pada react dihunakan constructor ini
    super(props); //PROPS DIBIKIN OPSIONAL , YG PENTING CONSTRUCTOR SUPER HARUS ADA
    this.state = {
      //disini ada 2 var, yaitu satu objek (formdata) dan satu boolean (isRequest), disini ada 2 method yaitu onSiignin dan textChanged

      formdata: {
        //<<<<<<< OBJEK
        username: "",
        password: "",
      },
      id: "",
      isRequest: false,
    };
    this.onSignIn = this.onSignIn.bind(this);
    this.textChanged = this.textChanged.bind(this);
  }
  textChanged(e) {
    // e ITU ARTINYA EVEN
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value; // e.target.name itu jika di textbox username maka brarti = tmp[e.username.name]
    this.setState({
      // nilai target di dapat dari property name. brarti mencari property name nya dimana
      formdata: tmp, // dari tmp dipindahkan kembali ke formdata dg this.setState formdata : tmp
    });
  }
  async onSignIn() {
    this.setState({
      isRequest: true, //pada onsign ini issRequest di set jadi true dari false.
    });

    let result = await API.login(
      this.state.formdata.username,
      this.state.formdata.password
    ); //pasangan async ialah await, agak tidak berbenturan kerjanya
    this.setState({
      id: result.message.userdata.id,
    });
    alert(this.state.id);
    // kalau tidak pakai await ia akan langsung menjalankan yg lain
    // alert(JSON.stringify(result.message.userdata.id));
    if (result.code === 200) {
      localStorage.setItem(
        config.LS.USERDATA,
        JSON.stringify(result.message.userdata)
      );
      localStorage.setItem(config.LS.USERNAME, this.state.formdata.username);
      localStorage.setItem(config.LS.ID_LOGIN, this.state.id);
      localStorage.setItem(config.LS.TOKEN, result.message.token);

      this.props.history.push("/userrole");
    } else {
      alert(result.message);
    }

    this.setState({
      isRequest: false,
    });
  }
  render() {
    return (
      <div className="hold-transition login-page">
        <div className="login-box">
          <div className="login-logo">
            <a href="#">
              <b>WebReact</b>Batch236
            </a>
          </div>
          <div className="card">
            <div className="card-body login-card-body">
              <p className="login-box-msg">MASUKAN USERNAME & PASSWORD</p>
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Username"
                  name="username"
                  required=""
                  autoFocus=""
                  value={this.state.username}
                  onChange={this.textChanged}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user"></span>
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Password"
                  name="password"
                  required=""
                  value={this.state.password}
                  onChange={this.textChanged}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                    className="btn btn-md btn-primary btn-block"
                    disabled={this.state.isRequest}
                    type="submit"
                    onClick={this.onSignIn}
                  >
                    Sign in
                  </button>
                </div>
              </div>

              <center>
                <p className="mb-1">
                  <a className="mt-5 mb-3 text-muted">
                    © Muhammad Abdul Latief
                  </a>
                </p>
              </center>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
