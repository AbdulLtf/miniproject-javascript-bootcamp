import apiconfig from "../configs/api.configs.json";
import React, { Component } from "react";
import List from "@material-ui/core/List";
import Collapse from "@material-ui/core/Collapse";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import menuItems from "./menuItems";
import axios from "axios";

const styles = {
  list: {
    width: 100,
  },
  links: {
    textDecoration: "none",
  },
  menuHeader: {
    paddingLeft: "10px",
  },
};

class MenuBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usermenu: [],
    };
  }
  componentDidMount() {
    this.getListMenu();
  }

  getListMenu() {
    var x = localStorage.getItem(apiconfig.LS.CHOICE);
    // alert(x)
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.USERMENU,
      method: "POST",
      headers: {
        Authorization: token,
      },
      data: { name: x },
    };
    axios(option)
      .then((response) => {
        if (response.data.code === 200) {
          let tmp = response.data.message;
          this.setState({
            usermenu: tmp,
          });
          this.props.history.push("/dashboard2");
        } else {
          // alert(response.data.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleClick(item) {
    this.setState((prevState) => ({ [item]: !prevState[item] }));
  }
  // menerima data dari JSON
  handler(data) {
    const { classes } = this.props;
    const { state } = this;
    return data.map((subOption) => {
      if (!subOption.children) {
        return (
          <div key={subOption.name}>
            <ListItem button key={subOption.name}>
              <Link to={subOption.url} className={classes.links}>
                <ListItemText inset primary={subOption.name} />
              </Link>
            </ListItem>
          </div>
        );
      }
      return (
        <div key={subOption.name}>
          <ListItem button onClick={() => this.handleClick(subOption.name)}>
            <ListItemText inset primary={subOption.name} />
            {state[subOption.name] ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={state[subOption.name]} timeout="auto" unmountOnExit>
            {this.handler(subOption.children)}
          </Collapse>
        </div>
      );
    });
  }
  render() {
    // alert(JSON.stringify(this.state.usermenu));
    const { classes, drawerOpen, menuOptions } = this.props;
    var x = localStorage.getItem(apiconfig.LS.USERNAME);
    return (
      <div
        style={{
          backgroundColor: "white",
          paddingRight: "50px",
          borderRadius: "5px",
          height: "500px",
          width: "250px",
          overflow: "auto",
        }}
      >
        <div
          style={{
            margin: "10px 0 -50px 15px",
          }}
        >
          <img
            src="dist/img/user1-128x128.jpg"
            alt="User Avatar"
            class="img-size-50 mr-3 img-circle"
          ></img>
        </div>
        <List>
          <ListItem
            key="menuHeading"
            divider
            disableGutters
            style={{ marginLeft: "60px" }}
          >
            <ListItemText
              className={classes.menuHeader}
              inset
              primary={"Hallo " + x}
            ></ListItemText>
          </ListItem>

          {this.handler(menuItems.data)}
        </List>
      </div>
    );
  }
}
export default withStyles(styles)(MenuBar);
