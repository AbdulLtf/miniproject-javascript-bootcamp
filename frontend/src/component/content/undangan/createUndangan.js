import apiconfig from "../../../configs/api.configs.json";
import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Container,
  Row,
  Col,
  Label,
  Input,
  Form,
  FormGroup,
} from "reactstrap";
import axios from "axios";
class CreateUndangan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      getRoTro: [],
      getJenUn: [],
      getPelamar: [],
      formdata: {
        tanggal: "",
        jam: "",
        pelamar: "",
        jenis_undangan: "",
        ro: "",
        tro: "",
        rotro: "",
        lokasi: "",
        catatan: "",
      },
    };
    this.submitHandler = this.submitHandler.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
  }
  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      formdata: tmp,
    });
  }

  submitHandler() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.CREATEUNDANGAN,
      method: "post",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
      data: this.state.formdata,
    };
    axios(option)
      .then((response) => {
        // console.log(this.state.formdata)
        // console.log(this.state.formdata)
        if (response.data.code === 200) {
          alert("data sudah dimasukan");
          this.props.history.push("/undangan");
        } else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
    this.props.closeModalHandler();
  }

  getRoTroJenUn() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETROTRO,
      method: "GET",
      headers: {
        Authorization: token,
      },
    };
    let option2 = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETJENUN,
      method: "GET",
      headers: {
        Authorization: token,
      },
    };
    let option3 = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.PELAMAR,
      method: "GET",
      headers: {
        Authorization: token,
      },
    };
    axios(option)
      .then((response) => {
        let tmp = [];
        response.data.message.map((row) => {
          tmp.push({
            id: row.id,
            fullnameRoTro: row.fullname,
          });
        });
        this.setState({
          getRoTro: tmp,
        });
      })
      .catch((error) => {
        console.log(error);
      });

    axios(option2)
      .then((response) => {
        let tmp = [];
        response.data.message.map((row) => {
          tmp.push({
            id: row.id,
            nameJenUn: row.name,
          });
        });
        this.setState({
          getJenUn: tmp,
        });
      })
      .catch((error) => {
        console.log(error);
      });
    axios(option3)
      .then((response) => {
        let tmp = [];
        response.data.message.map((row) => {
          tmp.push({
            id: row.id,
            namePelamar: row.fullname,
          });
        });
        this.setState({
          getPelamar: tmp,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getRoTroJenUn();
  }
  render() {
    //console.log(this.state.formdata)
    return (
      <Modal
        isOpen={this.props.create}
        className={this.props.className}
        size="lg"
      >
        <ModalHeader style={{ color: "#ffffff", backgroundColor: "#000066" }}>
          {" "}
          Buat Undangan
        </ModalHeader>
        <ModalBody>
          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">Tanggal Undangan*</Col>
            <Col width="200px">Jam*</Col>
          </Row>
          <Row>
            <Col style={{ width: "100px" }}>
              <Input
                type="date"
                class="form-control"
                name="tanggal"
                value={this.state.formdata.tanggal}
                onChange={this.changeHandler}
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <Input
                type="time"
                min="09:00"
                max="18:00"
                required
                class="form-control"
                name="jam"
                value={this.state.formdata.jam}
                onChange={this.changeHandler}
                reqiured
                placeholder="Waktu Undangan"
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>

          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">Pelamar *</Col>
            <Col width="200px">Jenis Undangan *</Col>
          </Row>
          <Row>
            <Col width="200px">
              <select
                className="form-control"
                name="pelamar"
                onChange={this.changeHandler}
                value={this.state.formdata.pelamar}
              >
                <option disabled value="">
                  Pilih
                </option>
                {this.state.getPelamar.map((row) => (
                  <option value={row.id}>{row.namePelamar}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <select
                className="form-control"
                name="jenis_undangan"
                onChange={this.changeHandler}
                value={this.state.formdata.jenis_undangan}
              >
                <option disabled value="">
                  Pilih
                </option>
                {this.state.getJenUn.map((row, x) => (
                  <option value={row.id}>{row.nameJenUn}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>
          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">RO *</Col>
            <Col width="200px">TRO*</Col>
          </Row>
          <Row>
            <Col width="200px">
              <select
                className="form-control"
                name="ro"
                onChange={this.changeHandler}
                value={this.state.formdata.ro}
              >
                <option disabled value="">
                  Pilih
                </option>
                {this.state.getRoTro.map((row, x) => (
                  <option value={row.id}>{row.fullnameRoTro}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <select
                className="form-control"
                name="tro"
                onChange={this.changeHandler}
                value={this.state.formdata.tro}
              >
                <option disabled value="">
                  Pilih
                </option>
                {this.state.getRoTro.map((row, x) => (
                  <option value={row.id}>{row.fullnameRoTro}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>

          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">RO/TRO Lain</Col>
            <Col width="200px">Lokasi</Col>
          </Row>
          <Row>
            <Col width="200px">
              <Input
                type="text"
                class="form-control"
                name="rotro"
                value={this.state.formdata.rotro}
                onChange={this.changeHandler}
                reqiured
                placeholder="Ro/Tro Lain"
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <Input
                type="text"
                class="form-control"
                name="lokasi"
                value={this.state.formdata.lokasi}
                onChange={this.changeHandler}
                reqiured
                placeholder="Lokasi undangan"
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>

          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">Catatan *</Col>
          </Row>
          <Row>
            <Col width="500px">
              <Input
                type="textarea"
                class="form-control"
                name="catatan"
                value={this.state.formdata.catatan}
                onChange={this.changeHandler}
                reqiured
                placeholder="Catatan untuk pelamar"
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.submitHandler}>
            add
          </Button>
          <Button color="warning" onClick={this.props.closeModalHandler}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
export default CreateUndangan;
