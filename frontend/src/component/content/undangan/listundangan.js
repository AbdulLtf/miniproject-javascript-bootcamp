import React from "react";
import apiconfig from "../../../configs/api.configs.json";
import axios from "axios";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Container,
  Row,
  Col,
  Label,
  Input,
  Form,
  FormGroup,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { Pagination } from "react-bootstrap";
import ViewUndangan from "./viewUndangan";
import DeleteUndangan from "./deleteUndangan";
import EditUndangan from "./editUndangan";
import CreateUndangan from "./createUndangan";
class listundangan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      undangan: [],
      undanganlist: [],
      currentUndangan: {},
      viewUndangan: false,
      deleteUndangan: false,
      editUndangan: false,
      showCreateUndangan: false,
      search1: "",
      begin: 0,
      end: 2,
      coba: 2,
    };
    this.getListUndanganAsc = this.getListUndanganAsc.bind(this);
    this.getListUndanganDesc = this.getListUndanganDesc.bind(this);
    this.showHandler = this.showHandler.bind(this);
    this.viewModalHandler = this.viewModalHandler.bind(this);
    this.editModalHandler = this.editModalHandler.bind(this);
    this.deleteModalHandler = this.deleteModalHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
    this.dropdownOpen = this.dropdownOpen.bind(this);
    this.dropdownClose = this.dropdownClose.bind(this);
    this.dropdownOpen1 = this.dropdownOpen1.bind(this);
    this.dropdownClose1 = this.dropdownClose1.bind(this);
    this.resetSearch = this.resetSearch.bind(this);
    this.next = this.next.bind(this);
  }

  resetSearch() {
    this.setState({
      search1: "",
      undangan: [],
      undanganlist: [],
      currentUndangan: {},
    });
  }
  dropdownClose() {
    this.setState({
      ascending: false,
    });
  }
  dropdownOpen() {
    this.setState({
      ascending: true,
    });
  }
  dropdownClose1() {
    this.setState({
      pagenation: false,
    });
  }
  dropdownOpen1() {
    this.setState({
      pagenation: true,
    });
  }
  updateSearch1(event) {
    this.setState({
      search1: event.target.value.substr(0, 20),
    });
  }
  closeModalHandler() {
    this.setState({
      viewUndangan: false,
      editUndangan: false,
      deleteUndangan: false,
      showCreateUndangan: false,
    });

    this.getListUndanganAsc();
    this.props.history.push("/undangan");
  }
  deleteModalHandler(idUD) {
    let tmp = {};
    // alert(idUD);
    this.state.undangan.map((row) => {
      if (idUD == row.id) {
        tmp = row;
      }
    });
    this.setState({
      currentUndangan: tmp,
      deleteUndangan: true,
    });
  }
  editModalHandler(idUD) {
    alert(idUD);
    this.state.undangan.map((row) => {
      if (idUD == row.id) {
        this.setState({
          currentUndangan: row,
          editUndangan: true,
        });
      }
    });
  }

  viewModalHandler(idUD) {
    let tmp = {};
    this.state.undangan.map((row) => {
      if (idUD == row.id) {
        tmp = row;
      }
    });
    this.setState({
      currentUndangan: tmp,
      viewUndangan: true,
    });
  }

  showHandler() {
    this.setState({ showCreateUndangan: true });
  }

  getListUndanganAsc() {
    // alert(this.state.search1);
    if (this.state.search1 == "") {
      alert("data tidak boleh kosong");
      this.setState({
        undangan: [],
      });
    } else {
      let token = localStorage.getItem(apiconfig.LS.TOKEN);
      let option = {
        url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETUNDANGANASC,
        method: "post",
        headers: {
          Authorization: token,
          "Content-Type": "application/json",
        },
        data: { search1: this.state.search1 },
      };
      axios(option)
        .then((response) => {
          if (response == null) {
            alert("tidak ada data");
          } else {
            let tmp = [];
            let tmp2 = [];
            response.data.message.map((row, x) => {
              let c = (
                <Link to="#">
                  <span
                    onClick={() => {
                      this.viewModalHandler(row.id);
                    }}
                    className="fa fa-search"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.editModalHandler(row.id);
                    }}
                    className="fa fa-edit"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.deleteModalHandler(row.id);
                    }}
                    className="fa fa-trash"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                </Link>
              );
              tmp.push({
                id: row.id,
                stp_id: row.schedule_type_id,
                pl_id: row.biodata_id,
                invitation_code: row.invitation_code,
                fullname: row.fullname,
                name: row.name,
                invitation_date: row.invitation_date,
                ro: row.ro,
                tro: row.tro,
                rotro: row.other_ro_tro,
                location: row.location,
                notes: row.notes,
                time: row.time,
              });
              tmp2.push({
                id: row.id,
                invitation_code: row.invitation_code,
                fullname: row.fullname,
                name: row.name,
                invitation_date: row.invitation_date,
                action: c,
                sekolah: row.school_name,
                jurusan: row.major,
                time: row.time,
              });
            });

            this.setState({
              undangan: tmp,
              undanganlist: tmp2,
            });
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  }

  getListUndanganDesc() {
    // alert(this.state.search1);
    if (this.state.search1 == "") {
      alert("data tidak boleh kosong");
      this.setState({
        undangan: [],
      });
    } else {
      let token = localStorage.getItem(apiconfig.LS.TOKEN);
      let option = {
        url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETUNDANGANDESC,
        method: "post",
        headers: {
          Authorization: token,
          "Content-Type": "application/json",
        },
        data: { search1: this.state.search1 },
      };
      axios(option)
        .then((response) => {
          if (response == null) {
            alert("tidak ada data");
          } else {
            let tmp = [];
            let tmp2 = [];
            response.data.message.map((row, x) => {
              let c = (
                <Link to="#">
                  <span
                    onClick={() => {
                      this.viewModalHandler(row.id);
                    }}
                    className="fa fa-search"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.editModalHandler(row.id);
                    }}
                    className="fa fa-edit"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                  <span
                    onClick={() => {
                      this.deleteModalHandler(row.id);
                    }}
                    className="fa fa-trash"
                    style={{ fontSize: "18px", paddingRight: "30px" }}
                  ></span>
                </Link>
              );
              tmp.push({
                id: row.id,
                stp_id: row.schedule_type_id,
                pl_id: row.biodata_id,
                invitation_code: row.invitation_code,
                fullname: row.fullname,
                name: row.name,
                invitation_date: row.invitation_date,
                ro: row.ro,
                tro: row.tro,
                rotro: row.other_ro_tro,
                location: row.location,
                notes: row.notes,
                time: row.time,
              });
              tmp2.push({
                id: row.id,
                invitation_code: row.invitation_code,
                fullname: row.fullname,
                name: row.name,
                invitation_date: row.invitation_date,
                action: c,
                sekolah: row.school_name,
                jurusan: row.major,
                time: row.time,
              });
            });

            this.setState({
              undangan: tmp,
              undanganlist: tmp2,
            });
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  }
  next() {
    let a = this.state.end;
    let b = this.state.coba;
    let c = this.state.undanganlist;
    let d = c.length;
    if (this.state.end < d) {
      this.setState({
        begin: a,
        end: a + b,
      });
    } else {
      this.setState({
        begin: this.state.begin,
        end: this.state.end,
      });
    }
  }
  previous() {
    let a = this.state.begin;
    let b = this.state.coba;
    if (this.state.begin > 0) {
      this.setState({
        begin: a - b,
        end: a,
      });
    } else {
      this.setState({
        begin: this.state.begin,
        end: this.state.end,
      });
    }
  }
  rows10() {
    this.setState({
      begin: 0,
      end: 10,
      coba: 10,
    });
  }
  rows20() {
    this.setState({
      begin: 0,
      end: 20,
      coba: 20,
    });
  }
  rows30() {
    this.setState({
      begin: 0,
      end: 30,
      coba: 30,
    });
  }
  rows40() {
    this.setState({
      begin: 0,
      end: 40,
      coba: 40,
    });
  }
  rows50() {
    this.setState({
      begin: 0,
      end: 50,
      coba: 50,
    });
  }
  rows1() {
    this.setState({
      begin: 0,
      end: 1,
      coba: 1,
    });
  }

  rows2() {
    this.setState({
      begin: 0,
      end: 2,
      coba: 2,
    });
  }

  rows5() {
    this.setState({
      begin: 0,
      end: 5,
      coba: 5,
    });
  }

  render() {
    var filteredResource = this.state.undanganlist.slice(
      this.state.begin,
      this.state.end
    );
    return (
      <div style={{ height: "212px", width: "100%" }}>
        <div
          class="container"
          style={{ marginTop: "10px", width: "100%", height: "50%" }}
        >
          <center>
            <div>
              <ol class="breadcrumb float-sm-right">
                <br></br>
                <li class="breadcrumb-item">
                  <a href="/dashboard2">Home</a>
                </li>
                <li class="breadcrumb-item active">List Undangan</li>
              </ol>
              <h4>LIST Undangan</h4>
            </div>
          </center>
          <CreateUndangan
            create={this.state.showCreateUndangan}
            closeModalHandler={this.closeModalHandler}
          />
          <ViewUndangan
            view={this.state.viewUndangan}
            closeModalHandler={this.closeModalHandler}
            undangan={this.state.currentUndangan}
          />
          <EditUndangan
            edit={this.state.editUndangan}
            closeModalHandler={this.closeModalHandler}
            undangan={this.state.currentUndangan}
          />
          <DeleteUndangan
            delete={this.state.deleteUndangan}
            closeModalHandler={this.closeModalHandler}
            undangan={this.state.currentUndangan}
          />
          <div className="jumbotron">
            <Row
              style={{
                borderBottomStyle: "ridge",
                borderBottomColor: "#000066",
              }}
            >
              <Col
                xs="auto"
                style={{
                  marginLeft: "10px",
                  marginTop: "-10px",
                  columnWidth: "250px",
                }}
              >
                <Row style={{ fontSize: "15px" }}>Pencarian</Row>
                <Row>
                  <Input
                    type="text"
                    class="form-control"
                    reqiured
                    placeholder="Berdasarkan Nama / Universitas"
                    value={this.state.search1}
                    onChange={this.updateSearch1.bind(this)}
                  />
                </Row>
              </Col>
              <Col
                xs="auto"
                style={{
                  marginLeft: "10px",
                  columnWidth: "20px",
                }}
              >
                <span
                  className="fa fa-search"
                  onClick={this.getListUndanganAsc}
                  style={{
                    fontSize: "25px",
                    paddingTop: "20px",
                  }}
                />
              </Col>
              <Col xs="auto" style={{ marginTop: "15px" }}>
                <Button
                  onClick={this.resetSearch}
                  style={{
                    backgroundColor: "#000066",
                    color: "#ffffff",
                  }}
                >
                  reset
                </Button>
              </Col>
              <Col xs="auto" style={{ marginTop: "15px", marginLeft: "350px" }}>
                <ButtonDropdown
                  isOpen={this.state.ascending}
                  onClick={this.dropdownOpen}
                  toggle={this.dropdownClose}
                >
                  <DropdownToggle
                    caret
                    style={{ backgroundColor: "#000066", color: "#ffffff" }}
                  >
                    AZ
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem
                      header
                      style={{ fontSize: "12px", float: "left" }}
                    >
                      Order
                    </DropdownItem>
                    <DropdownItem onClick={this.getListUndanganAsc}>
                      Ascending
                    </DropdownItem>
                    <DropdownItem onClick={this.getListUndanganDesc}>
                      Descending
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </Col>
              <Col xs="auto" style={{ marginTop: "15px" }}>
                <ButtonDropdown
                  isOpen={this.state.pagenation}
                  onClick={this.dropdownOpen1}
                  toggle={this.dropdownClose1}
                >
                  <DropdownToggle
                    caret
                    style={{ backgroundColor: "#000066", color: "#ffffff" }}
                  >
                    =
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem
                      header
                      style={{ fontSize: "12px", float: "left" }}
                    >
                      Row Per Page
                    </DropdownItem>
                    <DropdownItem onClick={this.rows1.bind(this)}>
                      1
                    </DropdownItem>
                    <DropdownItem onClick={this.rows2.bind(this)}>
                      2
                    </DropdownItem>
                    <DropdownItem onClick={this.rows5.bind(this)}>
                      5
                    </DropdownItem>
                    <DropdownItem onClick={this.rows10.bind(this)}>
                      10
                    </DropdownItem>
                    <DropdownItem onClick={this.rows20.bind(this)}>
                      20
                    </DropdownItem>
                    <DropdownItem onClick={this.rows30.bind(this)}>
                      30
                    </DropdownItem>
                    <DropdownItem onClick={this.rows40.bind(this)}>
                      40
                    </DropdownItem>
                    <DropdownItem onClick={this.rows50.bind(this)}>
                      50
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </Col>
              <Col xs="auto" style={{ marginTop: "10px" }}>
                <Button
                  onClick={this.showHandler}
                  style={{
                    backgroundColor: "#000066",
                    color: "#ffffff",
                    borderRadius: "40px",
                    fontSize: "20px",
                  }}
                >
                  +
                </Button>
              </Col>
            </Row>
            <Row>
              <table
                style={{ borderStyle: "insert" }}
                id="mytable"
                class="table table-bordered table-striped"
              >
                <thead>
                  <tr>
                    <th>
                      <center>Kode Undangan</center>
                    </th>
                    <th>
                      <center>Nama pelamar</center>
                    </th>
                    <th>
                      <center>Pendidikan</center>
                    </th>
                    <th>
                      <center>Jadwal Undangan</center>
                    </th>
                    <th>
                      <center>Action</center>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.undanganlist == "" ? (
                    <tr>
                      <td
                        style={{ color: "red", fontWeight: "bold" }}
                        colSpan="5"
                      >
                        <center>DATA TIDAK DITEMUKAN</center>
                      </td>
                    </tr>
                  ) : (
                    filteredResource.map((row, x) => (
                      <tr>
                        <td>
                          <center>{row.invitation_code}</center>
                        </td>
                        <td>
                          <center>{row.fullname}</center>
                        </td>
                        <td>
                          <center>
                            {row.sekolah}/{row.jurusan}
                          </center>
                        </td>
                        <td>
                          <center>
                            {row.invitation_date}/{row.time}
                          </center>
                        </td>
                        <td>
                          <center>{row.action}</center>
                        </td>
                      </tr>
                    ))
                  )}
                </tbody>
              </table>
            </Row>
            <button class="float-right" onClick={this.next}>
              Next
            </button>
            <button class="float-right" onClick={this.previous.bind(this)}>
              {" "}
              Previous{" "}
            </button>
            <Pagination bsSize="medium" items={1} activePage={1} />
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(listundangan);

// // backup
// import React from "react";
// import apiconfig from "../../../configs/api.configs.json";
// import axios from "axios";
// import { Link } from "react-router-dom";
// import ViewUndangan from "./viewUndangan";
// import DeleteUndangan from "./deleteUndangan";
// import EditUndangan from "./editUndangan";
// import CreateUndangan from "./createUndangan";
// import { MDBDataTable } from "mdbreact";
// class listundangan extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       undangan: [],
//       undanganlist: [],
//       currentUndangan: {},
//       viewUndangan: false,
//       deleteUndangan: false,
//       editUndangan: false,
//       showCreateUndangan: false,
//     };
// this.showHandler = this.showHandler.bind(this);
// this.viewModalHandler = this.viewModalHandler.bind(this);
// this.editModalHandler = this.editModalHandler.bind(this);
// this.deleteModalHandler = this.deleteModalHandler.bind(this);
// this.closeModalHandler = this.closeModalHandler.bind(this);
//   }
// closeModalHandler() {
//   this.setState({
//     viewUndangan: false,
//     editUndangan: false,
//     deleteUndangan: false,
//     showCreateUndangan: false,
//   });
//   this.getListUndangan();
//   this.getListUndangan();
//   this.getListUndangan();
//   this.getListUndangan();
//   this.getListUndangan();
//   this.getListUndangan();
//   this.getListUndangan();
// }

// deleteModalHandler(idUD) {
//   let tmp = {};
//   // alert(idUD);
//   this.state.undangan.map((row) => {
//     if (idUD == row.id) {
//       tmp = row;
//     }
//   });
//   this.setState({
//     currentUndangan: tmp,
//     deleteUndangan: true,
//   });
// }
// editModalHandler(idUD) {
//   alert(idUD);
//   this.state.undangan.map((row) => {
//     if (idUD == row.id) {
//       this.setState({
//         currentUndangan: row,
//         editUndangan: true,
//       });
//     }
//   });
// }

// viewModalHandler(idUD) {
//   let tmp = {};
//   this.state.undangan.map((row) => {
//     if (idUD == row.id) {
//       tmp = row;
//     }
//   });
//   this.setState({
//     currentUndangan: tmp,
//     viewUndangan: true,
//   });
// }

// showHandler() {
//   this.setState({ showCreateUndangan: true });
// }
// getListUndangan() {
//   let token = localStorage.getItem(apiconfig.LS.TOKEN);
//   let option = {
//     url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETUNDANGAN,
//     method: "get",
//     headers: {
//       Authorization: token,
//     },
//   };
//     axios(option)
//       .then((response) => {
//         let tmp = [];
//         let tmp2 = [];
//         response.data.message.map((row, x) => {
//           let c = (
//             <Link to="#">
//               <span
//                 onClick={() => {
//                   this.viewModalHandler(row.id);
//                 }}
//                 className="fa fa-search"
//                 style={{ fontSize: "18px", paddingRight: "30px" }}
//               ></span>
//               <span
//                 onClick={() => {
//                   this.editModalHandler(row.id);
//                 }}
//                 className="fa fa-edit"
//                 style={{ fontSize: "18px", paddingRight: "30px" }}
//               ></span>
//               <span
//                 onClick={() => {
//                   this.deleteModalHandler(row.id);
//                 }}
//                 className="fa fa-trash"
//                 style={{ fontSize: "18px", paddingRight: "30px" }}
//               ></span>
//             </Link>
//           );
//           tmp.push({
//             id: row.id,
//             invitation_code: row.invitation_code,
//             fullname: row.fullname,
//             name: row.name,
//             invitation_date: row.invitation_date,
//             ro: row.ro,
//             tro: row.tro,
//             rotro: row.other_ro_tro,
//             location: row.location,
//             notes: row.notes,
//             time: row.time,
//           });
//           tmp2.push({
//             id: row.id,
//             invitation_code: row.invitation_code,
//             fullname: row.fullname,
//             name: row.name,
//             invitation_date: row.invitation_date,
//             action: c,
//           });
//         });

//         this.setState({
//           undangan: tmp,
//           undanganlist: tmp2,
//         });
//       })
//       .catch((error) => {
//         alert(error);
//       });
//   }

//   componentDidMount() {
//     this.getListUndangan();
//   }

//   render() {
//     const data = {
//       columns: [
//         {
//           label: "Kode Undangan",
//           field: "invitation_code",
//           sort: "asc",
//           width: 270,
//         },
//         {
//           label: "Nama Pelamar",
//           field: "fullname",
//           sort: "asc",
//           width: 200,
//         },
//         {
//           label: "Jenis Undangan",
//           field: "name",
//           sort: "asc",
//           width: 100,
//         },
//         {
//           label: "Tanggal Undangan",
//           field: "invitation_date",
//           sort: "asc",
//           width: 150,
//         },
//         {
//           label: "Action",
//           field: "action",
//           sort: "asc",
//           width: 100,
//         },
//       ],
//       rows: this.state.undanganlist,
//     };

//     return (
//       <div style={{ height: "212px", width: "100%" }}>
//         <div
//           class="container"
//           style={{ marginTop: "10px", width: "100%", height: "50%" }}
//         >
//           <center>
//             <div>
//               <ol class="breadcrumb float-sm-right">
//                 <br></br>
//                 <li class="breadcrumb-item">
//                   <a href="/dashboard2">Home</a>
//                 </li>
//                 <li class="breadcrumb-item active">List Undangan</li>
//               </ol>
//               <h4>LIST Undangan</h4>
//             </div>
//           </center>

// <CreateUndangan
//   create={this.state.showCreateUndangan}
//   closeModalHandler={this.closeModalHandler}
// />
// <ViewUndangan
//   view={this.state.viewUndangan}
//   closeModalHandler={this.closeModalHandler}
//   undangan={this.state.currentUndangan}
// />
// <EditUndangan
//   edit={this.state.editUndangan}
//   closeModalHandler={this.closeModalHandler}
//   undangan={this.state.currentUndangan}
// />
// <DeleteUndangan
//   delete={this.state.deleteUndangan}
//   closeModalHandler={this.closeModalHandler}
//   undangan={this.state.currentUndangan}
// />

//           <div className="jumbotron">
//             <button
//               class="btn btn-primary btn-sm float-right"
//               onClick={this.showHandler}
//               style={{ marginTop: "1%", marginRight: "2%" }}
//             >
//               <i class="fas fa-user-plus"> </i>
//               <span>Add</span>
//             </button>
//             <MDBDataTable
//               striped
//               bordered
//               hover
//               paginationLabel={["Sebelumnya", "Selanjutnya"]}
//               noBottomColumns
//               responsive
//               searchLabel="Cari Data"
//               data={data}
//             />
//           </div>
//         </div>
//       </div>
//     );
//   }
// }
// export default listundangan;
