import React from "react";
import apiconfig from "../../../configs/api.configs.json";
import axios from "axios";
import { Link } from "react-router-dom";
import ViewUndangan from "./viewUndangan";
import DeleteUndangan from "./deleteUndangan";
import EditUndangan from "./editUndangan";
import CreateUndangan from "./createUndangan";
import { MDBDataTable } from "mdbreact";
class listundangan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      undangan: [],
      undanganlist: [],
      currentUndangan: {},
      viewUndangan: false,
      deleteUndangan: false,
      editUndangan: false,
      showCreateUndangan: false,
    };
    this.showHandler = this.showHandler.bind(this);
    this.viewModalHandler = this.viewModalHandler.bind(this);
    this.editModalHandler = this.editModalHandler.bind(this);
    this.deleteModalHandler = this.deleteModalHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
  }
  closeModalHandler() {
    this.setState({
      viewUndangan: false,
      editUndangan: false,
      deleteUndangan: false,
      showCreateUndangan: false,
    });
    this.getListUndangan();
    this.getListUndangan();
    this.getListUndangan();
    this.getListUndangan();
    this.getListUndangan();
    this.getListUndangan();
    this.getListUndangan();
  }

  deleteModalHandler(idUD) {
    let tmp = {};
    // alert(idUD);
    this.state.undangan.map((row) => {
      if (idUD == row.id) {
        tmp = row;
      }
    });
    this.setState({
      currentUndangan: tmp,
      deleteUndangan: true,
    });
  }
  editModalHandler(idUD) {
    alert(idUD);
    this.state.undangan.map((row) => {
      if (idUD == row.id) {
        this.setState({
          currentUndangan: row,
          editUndangan: true,
        });
      }
    });
  }

  viewModalHandler(idUD) {
    let tmp = {};
    this.state.undangan.map((row) => {
      if (idUD == row.id) {
        tmp = row;
      }
    });
    this.setState({
      currentUndangan: tmp,
      viewUndangan: true,
    });
  }

  showHandler() {
    this.setState({ showCreateUndangan: true });
  }
  getListUndangan() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETUNDANGAN,
      method: "get",
      headers: {
        Authorization: token,
      },
    };
    axios(option)
      .then((response) => {
        let tmp = [];
        let tmp2 = [];
        response.data.message.map((row, x) => {
          let c = (
            <Link to="#">
              <span
                onClick={() => {
                  this.viewModalHandler(row.id);
                }}
                className="fa fa-search"
                style={{ fontSize: "18px", paddingRight: "30px" }}
              ></span>
              <span
                onClick={() => {
                  this.editModalHandler(row.id);
                }}
                className="fa fa-edit"
                style={{ fontSize: "18px", paddingRight: "30px" }}
              ></span>
              <span
                onClick={() => {
                  this.deleteModalHandler(row.id);
                }}
                className="fa fa-trash"
                style={{ fontSize: "18px", paddingRight: "30px" }}
              ></span>
            </Link>
          );
          tmp.push({
            id: row.id,
            invitation_code: row.invitation_code,
            fullname: row.fullname,
            name: row.name,
            invitation_date: row.invitation_date,
            ro: row.ro,
            tro: row.tro,
            rotro: row.other_ro_tro,
            location: row.location,
            notes: row.notes,
            time: row.time,
          });
          tmp2.push({
            id: row.id,
            invitation_code: row.invitation_code,
            fullname: row.fullname,
            name: row.name,
            invitation_date: row.invitation_date,
            action: c,
          });
        });

        this.setState({
          undangan: tmp,
          undanganlist: tmp2,
        });
      })
      .catch((error) => {
        alert(error);
      });
  }

  componentDidMount() {
    this.getListUndangan();
  }

  render() {
    const data = {
      columns: [
        {
          label: "Kode Undangan",
          field: "invitation_code",
          sort: "asc",
          width: 270,
        },
        {
          label: "Nama Pelamar",
          field: "fullname",
          sort: "asc",
          width: 200,
        },
        {
          label: "Jenis Undangan",
          field: "name",
          sort: "asc",
          width: 100,
        },
        {
          label: "Tanggal Undangan",
          field: "invitation_date",
          sort: "asc",
          width: 150,
        },
        {
          label: "Action",
          field: "action",
          sort: "asc",
          width: 100,
        },
      ],
      rows: this.state.undanganlist,
    };

    return (
      <div style={{ height: "212px", width: "100%" }}>
        <div
          class="container"
          style={{ marginTop: "10px", width: "100%", height: "50%" }}
        >
          <center>
            <div>
              <ol class="breadcrumb float-sm-right">
                <br></br>
                <li class="breadcrumb-item">
                  <a href="/dashboard2">Home</a>
                </li>
                <li class="breadcrumb-item active">List Undangan</li>
              </ol>
              <h4>LIST Undangan</h4>
            </div>
          </center>

          <CreateUndangan
            create={this.state.showCreateUndangan}
            closeModalHandler={this.closeModalHandler}
          />
          <ViewUndangan
            view={this.state.viewUndangan}
            closeModalHandler={this.closeModalHandler}
            undangan={this.state.currentUndangan}
          />
          <EditUndangan
            edit={this.state.editUndangan}
            closeModalHandler={this.closeModalHandler}
            undangan={this.state.currentUndangan}
          />
          <DeleteUndangan
            delete={this.state.deleteUndangan}
            closeModalHandler={this.closeModalHandler}
            undangan={this.state.currentUndangan}
          />

          <div className="jumbotron">
            <button
              class="btn btn-primary btn-sm float-right"
              onClick={this.showHandler}
              style={{ marginTop: "1%", marginRight: "2%" }}
            >
              <i class="fas fa-user-plus"> </i>
              <span>Add</span>
            </button>
            <MDBDataTable
              striped
              bordered
              hover
              paginationLabel={["Sebelumnya", "Selanjutnya"]}
              noBottomColumns
              responsive
              searchLabel="Cari Data"
              data={data}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default listundangan;
