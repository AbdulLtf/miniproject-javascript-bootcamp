import apiconfig from "../../../configs/api.configs.json";
import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Container,
  Row,
  Col,
  Label,
  Input,
  Form,
  FormGroup,
} from "reactstrap";
import axios from "axios";
class EditUndangan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formdata: {
        id: "",
        tanggal: "",
        jam: "",
        pl_id: "",
        stp_id: "",
        ro: "",
        tro: "",
        rotro: "",
        location: "",
        notes: "",
      },
      udgid: "",
      getRoTro: [],
      getJenUn: [],
      getPelamar: [],
    };
    this.submitHandler = this.submitHandler.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
  }
  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      formdata: tmp,
    });
  }

  submitHandler() {
    alert(JSON.stringify(this.state.formdata));
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.EDITUNDANGAN,
      method: "put",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
      data: this.state.formdata,
    };
    let option2 = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.EDITUNDANGANDETAIL,
      method: "put",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
      data: this.state.formdata,
    };
    axios(option)
      .then((response) => {
        alert("success");
        if (response.data.code === 200) {
          alert("data sudah dimasukan");
          this.props.history.push("/undangan");
        } else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
    axios(option2)
      .then((response) => {
        if (response.data.code === 200) {
          alert("data sudah dimasukan");
          this.props.history.push("/undangan");
        } else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
    this.props.closeModalHandler();
  }

  getRoTroJenun() {
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETROTRO,
      method: "GET",
      headers: {
        Authorization: token,
      },
    };
    let option2 = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETJENUN,
      method: "GET",
      headers: {
        Authorization: token,
      },
    };
    let option3 = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.PELAMAR,
      method: "GET",
      headers: {
        Authorization: token,
      },
    };
    axios(option)
      .then((response) => {
        let tmp = [];
        response.data.message.map((row) => {
          tmp.push({
            id: row.id,
            fullnameRoTro: row.fullname,
          });
        });
        this.setState({
          getRoTro: tmp,
        });
      })
      .catch((error) => {
        console.log(error);
      });

    axios(option2)
      .then((response) => {
        let tmp = [];
        response.data.message.map((row) => {
          tmp.push({
            id: row.id,
            nameJenUn: row.name,
          });
        });
        this.setState({
          getJenUn: tmp,
        });
      })
      .catch((error) => {
        console.log(error);
      });
    axios(option3)
      .then((response) => {
        let tmp = [];
        response.data.message.map((row) => {
          tmp.push({
            id: row.id,
            namePelamar: row.fullname,
          });
        });
        this.setState({
          getPelamar: tmp,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  componentWillReceiveProps(newProps) {
    this.setState({
      formdata: newProps.undangan,
    });
  }
  componentDidMount() {
    this.getRoTroJenun();
  }
  render() {
    //console.log(this.state.formdata)
    return (
      <Modal
        isOpen={this.props.edit}
        className={this.props.className}
        size="lg"
      >
        <ModalHeader style={{ color: "#ffffff", backgroundColor: "#000066" }}>
          {" "}
          Edit Undangan
        </ModalHeader>
        <ModalBody>
          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">Tanggal Undangan*</Col>
            <Col width="200px">Jam*</Col>
          </Row>
          <Row>
            <Col style={{ width: "100px" }}>
              <Input
                type="date"
                class="form-control"
                name="tanggal"
                value={this.state.formdata.tanggal}
                onChange={this.changeHandler}
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <Input
                type="text"
                class="form-control"
                name="jam"
                value={this.state.formdata.jam}
                onChange={this.changeHandler}
                reqiured
                placeholder={this.props.undangan.time}
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>

          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">Pelamar *</Col>
            <Col width="200px">Jenis Undangan *</Col>
          </Row>
          <Row>
            <Col width="200px">
              <select
                className="form-control"
                name="pl_id"
                onChange={this.changeHandler}
                value={this.state.formdata.pl_id}
              >
                <option disabled selected value={this.props.undangan.pl_id}>
                  {this.props.undangan.fullname}
                </option>
                {this.state.getPelamar.map((row) => (
                  <option value={row.id}>{row.namePelamar}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <select
                className="form-control"
                name="stp_id"
                onChange={this.changeHandler}
                value={this.state.formdata.stp_id}
              >
                <option disabled selected value={this.props.stp_id}>
                  {this.props.undangan.name}
                </option>
                {this.state.getJenUn.map((row, x) => (
                  <option value={row.id}>{row.nameJenUn}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>
          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">RO *</Col>
            <Col width="200px">TRO*</Col>
          </Row>
          <Row>
            <Col width="200px">
              <select
                className="form-control"
                name="ro"
                onChange={this.changeHandler}
                value={this.state.formdata.ro}
              >
                {/* <option disabled value="">
                  {this.props.undangan.ro}
                </option> */}
                {this.state.getRoTro.map((row, x) => (
                  <option value={row.id}>{row.fullnameRoTro}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <select
                className="form-control"
                name="tro"
                onChange={this.changeHandler}
                value={this.state.formdata.tro}
              >
                {/* <option disabled value="">
                  {this.props.undangan.tro}
                </option> */}
                {this.state.getRoTro.map((row, x) => (
                  <option value={row.id}>{row.fullnameRoTro}</option>
                ))}
              </select>
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>

          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">RO/TRO Lain</Col>
            <Col width="200px">Lokasi</Col>
          </Row>
          <Row>
            <Col width="200px">
              <Input
                type="text"
                class="form-control"
                name="rotro"
                value={this.state.formdata.rotro}
                onChange={this.changeHandler}
                reqiured
                placeholder={this.props.undangan.rotro}
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
            <Col width="200px">
              <Input
                type="text"
                class="form-control"
                name="location"
                value={this.props.undangan.location}
                onChange={this.changeHandler}
                reqiured
                placeholder={this.props.undangan.location}
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>

          <Row style={{ fontStyle: "italic", fontSize: "10px" }}>
            <Col width="200px">Catatan *</Col>
          </Row>
          <Row>
            <Col width="500px">
              <Input
                type="textarea"
                class="form-control"
                name="notes"
                value={this.props.undangan.notes}
                onChange={this.changeHandler}
                reqiured
                placeholder={this.props.undangan.notes}
              />
              <Label
                for="text"
                style={{
                  color: "#ff0000",
                  fontSize: "14px",
                  fontStyle: "italic",
                }}
              ></Label>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.submitHandler}>
            Ubah
          </Button>
          <Button color="warning" onClick={this.props.closeModalHandler}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
export default EditUndangan;
