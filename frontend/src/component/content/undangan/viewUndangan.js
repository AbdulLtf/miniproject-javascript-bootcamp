import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import apiconfig from "../../../configs/api.configs.json";
import axios from "axios";
import EditUndangan from "./editUndangan";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";

class ViewUndangan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nameRo: "",
      nameTro: "",
      editUndangan: false,
      undangan: {},
    };
    this.ubah = this.ubah.bind(this);
    this.editModalHandler = this.editModalHandler.bind(this);
    this.editCloseModalHandler = this.editCloseModalHandler.bind(this);
  }
  ubah() {
    alert(JSON.stringify(this.state.undangan));
    this.props.closeModalHandler();
    this.editModalHandler();
  }
  editCloseModalHandler() {
    this.setState({
      editUndangan: false,
    });
    this.props.history.push("/undangan");
  }

  editModalHandler() {
    this.setState({
      editUndangan: true,
    });
  }
  componentWillReceiveProps(newProps) {
    this.setState({
      undangan: newProps.undangan,
    });
  }

  // get(undangan) {
  //   this.setState({
  //     nameRo: this.props.undangan.ro,
  //     nameTro: this.props.undangan.tro,
  //   });
  // }
  getNameRoTro() {
    // // alert(JSON.stringify(this.state.formdata));
    // alert(this.state.nameRo);
    // alert(this.state.nameTro);
    // let token = localStorage.getItem(apiconfig.LS.TOKEN);
    // let option = {
    //   url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.NAMAROTRO,
    //   method: "POST",
    //   headers: {
    //     Authorization: token,
    //   },
    //   data: { id: this.state.formdata.tro },
    // };
    // let option2 = {
    //   url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.NAMAROTRO,
    //   method: "POST",
    //   headers: {
    //     Authorization: token,
    //   },
    //   data: { id: this.state.formdata.tro },
    // };
    // axios(option)
    //   .then((response) => {
    //     this.setState({
    //       nameRo: response.data.message,
    //     });
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
    // axios(option2)
    //   .then((response) => {
    //     this.setState({
    //       nameTro: response.data.message,
    //     });
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
    // alert(this.state.nameRo);
    // alert(this.state.nameTro);
  }

  componentDidMount() {
    // this.get();
    this.getNameRoTro();
  }
  render() {
    return (
      <div>
        <EditUndangan
          edit={this.state.editUndangan}
          closeModalHandler={this.editCloseModalHandler}
          undangan={this.state.undangan}
        />
        <Modal
          isOpen={this.props.view}
          className={this.props.className}
          size="lg"
        >
          <ModalHeader
            className="custom-modal-style"
            style={{ color: "#ffffff", backgroundColor: "#000066" }}
          >
            Undangan {this.props.undangan.invitation_code}
          </ModalHeader>
          <ModalBody>
            <div className="container">
              <div className="row" style={{ width: "710px" }}>
                <div className="mb-2 col-lg-8" style={{ display: "block" }}>
                  <p className="mr-3">
                    Tanggal Undangan&ensp;:&ensp;{" "}
                    {this.props.undangan.invitation_date}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    Jam &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;:&ensp;{" "}
                    {this.props.undangan.time}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    Pelamar&emsp;&emsp;&emsp;&emsp;&ensp;&nbsp;:&ensp;{" "}
                    {this.props.undangan.fullname}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    Jenis Undangan&emsp;&ensp;:&ensp; {this.props.undangan.name}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    RO&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;:&ensp;{" "}
                    {this.props.undangan.ro}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    TRO&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;:&ensp;{" "}
                    {this.props.undangan.tro}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    RO / TRO Lain&emsp;&emsp;&ensp;:&ensp;{" "}
                    {this.props.undangan.rotro}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    Lokasi&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;:&ensp;{" "}
                    {this.props.undangan.location}
                  </p>
                  <hr style={{ marginTop: "-7px", marginBottom: "3px" }} />
                  <p className="mr-3">
                    Catatan&emsp;&emsp;&emsp;&emsp;&emsp;:&ensp;{" "}
                    {this.props.undangan.notes}
                  </p>
                </div>
                <div className="mb-2 col-lg-4">
                  <img
                    src="dist/img/envelope.png"
                    style={{
                      width: "250px",
                      marginTop: "15%",
                      marginLeft: "20px",
                    }}
                    class="brand-image "
                  />
                </div>
              </div>
            </div>
          </ModalBody>
          <ModalFooter style={{ backgroundColor: "#e9ecef" }}>
            <Button color="primary" onClick={this.ubah}>
              Ubah
            </Button>
            <Button color="danger" onClick={this.props.closeModalHandler}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default withRouter(ViewUndangan);
