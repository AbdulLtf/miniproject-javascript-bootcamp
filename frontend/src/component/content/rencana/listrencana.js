import React from "react";
import apiconfig from "../../../configs/api.configs.json";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Container,
  Row,
  Col,
  Label,
  Input,
  Form,
  FormGroup,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import axios from "axios";
import { Link } from "react-router-dom";
import { Pagination } from "react-bootstrap";

//import {Alert} from 'reactstrap'

class list extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rencana: [],
      ascending: false,
      pagenation: false,
      search1: "",
      search2: "",
      begin: 0,
      end: 2,
      coba: 2,
    };
    this.getListRencanaAsc = this.getListRencanaAsc.bind(this);
    this.getListRencanaDesc = this.getListRencanaDesc.bind(this);
    this.dropdownOpen = this.dropdownOpen.bind(this);
    this.dropdownClose = this.dropdownClose.bind(this);
    this.dropdownOpen1 = this.dropdownOpen1.bind(this);
    this.dropdownClose1 = this.dropdownClose1.bind(this);
    this.resetSearch = this.resetSearch.bind(this);
    this.next = this.next.bind(this);
  }
  componentDidMount() {
    // this.getListRencana();
  }
  resetSearch() {
    this.setState({
      search1: "",
      search2: "",
      rencana: [],
    });
  }
  dropdownClose() {
    this.setState({
      ascending: false,
    });
  }
  dropdownOpen() {
    this.setState({
      ascending: true,
    });
  }
  dropdownClose1() {
    this.setState({
      pagenation: false,
    });
  }
  dropdownOpen1() {
    this.setState({
      pagenation: true,
    });
  }

  updateSearch1(event) {
    this.setState({
      search1: event.target.value.substr(0, 20),
    });
  }
  updateSearch2(event) {
    this.setState({
      search2: event.target.value.substr(0, 20),
    });
  }

  getListRencanaAsc() {
    if (
      (this.state.search1 == "" && this.state.search2 == "") ||
      this.state.search2 == "" ||
      this.state.search1 == ""
    ) {
      alert("data tidak boleh kosong");
      this.setState({
        rencana: [],
      });
    } else {
      if (this.state.search1 <= this.state.search2) {
        let token = localStorage.getItem(apiconfig.LS.TOKEN);
        let option = {
          url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.LISTRENCANAASC,
          method: "post",
          header: {
            Authorization: token,
            "Content-Type": "application/json",
          },
          data: { search1: this.state.search1, search2: this.state.search2 },
        };
        axios(option)
          .then((response) => {
            if (response == null) {
              alert("tidak ada data");
            } else {
              this.setState({
                rencana: response.data.message,
              });
            }
          })
          .catch((error) => {
            alert(error);
          });
      } else {
        alert("tanggal dari tidak boleh lebih besar");
      }
    }
  }
  getListRencanaDesc() {
    if (
      (this.state.search1 == "" && this.state.search2 == "") ||
      this.state.search2 == "" ||
      this.state.search1 == ""
    ) {
      alert("data tidak boleh kosong");
      this.setState({
        rencana: [],
      });
    } else {
      if (this.state.search1 <= this.state.search2) {
        let token = localStorage.getItem(apiconfig.LS.TOKEN);
        let option = {
          url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.LISTRENCANADESC,
          method: "post",
          header: {
            Authorization: token,
            "Content-Type": "application/json",
          },
          data: { search1: this.state.search1, search2: this.state.search2 },
        };
        axios(option)
          .then((response) => {
            if (response == null) {
              alert("tidak ada data");
            } else {
              this.setState({
                rencana: response.data.message,
              });
            }
          })
          .catch((error) => {
            alert(error);
          });
      } else {
        alert("tanggal dari tidak boleh lebih besar");
      }
    }
  }

  next() {
    let a = this.state.end;
    let b = this.state.coba;
    let c = this.state.rencana;
    let d = c.length;
    if (this.state.end < d) {
      this.setState({
        begin: a,
        end: a + b,
      });
    } else {
      this.setState({
        begin: this.state.begin,
        end: this.state.end,
      });
    }
  }
  previous() {
    let a = this.state.begin;
    let b = this.state.coba;
    if (this.state.begin > 0) {
      this.setState({
        begin: a - b,
        end: a,
      });
    } else {
      this.setState({
        begin: this.state.begin,
        end: this.state.end,
      });
    }
  }
  rows10() {
    this.setState({
      begin: 0,
      end: 10,
      coba: 10,
    });
  }
  rows20() {
    this.setState({
      begin: 0,
      end: 20,
      coba: 20,
    });
  }
  rows30() {
    this.setState({
      begin: 0,
      end: 30,
      coba: 30,
    });
  }
  rows40() {
    this.setState({
      begin: 0,
      end: 40,
      coba: 40,
    });
  }
  rows50() {
    this.setState({
      begin: 0,
      end: 50,
      coba: 50,
    });
  }
  rows1() {
    this.setState({
      begin: 0,
      end: 1,
      coba: 1,
    });
  }

  rows2() {
    this.setState({
      begin: 0,
      end: 2,
      coba: 2,
    });
  }

  rows5() {
    this.setState({
      begin: 0,
      end: 5,
      coba: 5,
    });
  }
  render() {
    var filteredResource = this.state.rencana.slice(
      this.state.begin,
      this.state.end
    );
    return (
      <div style={{ height: "212px", width: "100%" }}>
        <div
          class="container"
          style={{ marginTop: "10px", width: "100%", height: "50%" }}
        >
          <center>
            <div>
              <ol class="breadcrumb float-sm-right">
                <br></br>
                <li class="breadcrumb-item">
                  <a href="/dashboard2">Home</a>
                </li>
                <li class="breadcrumb-item active">List Rencana</li>
              </ol>
              <h4>LIST Rencana</h4>
            </div>
          </center>
          <div className="jumbotron">
            <Row
              style={{
                borderBottomStyle: "ridge",
                borderBottomColor: "#000066",
              }}
            >
              <Col
                xs="auto"
                style={{ marginLeft: "10px", columnWidth: "250px" }}
              >
                <Row style={{ fontSize: "10px" }}>Date From</Row>
                <Row>
                  <Input
                    type="date"
                    class="form-control"
                    reqiured
                    placeholder="From"
                    value={this.state.search1}
                    onChange={this.updateSearch1.bind(this)}
                  />
                </Row>
              </Col>
              <Col
                xs="auto"
                style={{ marginLeft: "10px", columnWidth: "250px" }}
              >
                <Row style={{ fontSize: "10px" }}>Date Until</Row>
                <Row>
                  <Input
                    type="Date"
                    class="form-control"
                    reqiured
                    placeholder="After"
                    value={this.state.search2}
                    onChange={this.updateSearch2.bind(this)}
                  />
                </Row>
              </Col>
              <Col
                xs="auto"
                style={{
                  marginLeft: "10px",
                  columnWidth: "20px",
                }}
              >
                <span
                  className="fa fa-search"
                  onClick={this.getListRencanaAsc}
                  style={{
                    fontSize: "25px",
                    paddingTop: "20px",
                  }}
                />
              </Col>
              <Col xs="auto" style={{ marginTop: "15px" }}>
                <Button
                  onClick={this.resetSearch}
                  style={{
                    backgroundColor: "#000066",
                    color: "#ffffff",
                  }}
                >
                  reset
                </Button>
              </Col>
              <Col xs="auto" style={{ marginTop: "15px", marginLeft: "100px" }}>
                <ButtonDropdown
                  isOpen={this.state.ascending}
                  onClick={this.dropdownOpen}
                  toggle={this.dropdownClose}
                >
                  <DropdownToggle
                    caret
                    style={{ backgroundColor: "#000066", color: "#ffffff" }}
                  >
                    AZ
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem
                      header
                      style={{ fontSize: "12px", float: "left" }}
                    >
                      Order
                    </DropdownItem>
                    <DropdownItem onClick={this.getListRencanaAsc}>
                      Ascending
                    </DropdownItem>
                    <DropdownItem onClick={this.getListRencanaDesc}>
                      Descending
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </Col>
              <Col xs="auto" style={{ marginTop: "15px" }}>
                <ButtonDropdown
                  isOpen={this.state.pagenation}
                  onClick={this.dropdownOpen1}
                  toggle={this.dropdownClose1}
                >
                  <DropdownToggle
                    caret
                    style={{ backgroundColor: "#000066", color: "#ffffff" }}
                  >
                    =
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem
                      header
                      style={{ fontSize: "12px", float: "left" }}
                    >
                      Row Per Page
                    </DropdownItem>
                    <DropdownItem onClick={this.rows1.bind(this)}>
                      1
                    </DropdownItem>
                    <DropdownItem onClick={this.rows2.bind(this)}>
                      2
                    </DropdownItem>
                    <DropdownItem onClick={this.rows5.bind(this)}>
                      5
                    </DropdownItem>
                    <DropdownItem onClick={this.rows10.bind(this)}>
                      10
                    </DropdownItem>
                    <DropdownItem onClick={this.rows20.bind(this)}>
                      20
                    </DropdownItem>
                    <DropdownItem onClick={this.rows30.bind(this)}>
                      30
                    </DropdownItem>
                    <DropdownItem onClick={this.rows40.bind(this)}>
                      40
                    </DropdownItem>
                    <DropdownItem onClick={this.rows50.bind(this)}>
                      50
                    </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </Col>
              <Col xs="auto" style={{ marginTop: "10px" }}>
                <Button
                  style={{
                    backgroundColor: "#000066",
                    color: "#ffffff",
                    borderRadius: "40px",
                    fontSize: "20px",
                  }}
                >
                  +
                </Button>
              </Col>
            </Row>
            <Row>
              <table
                style={{ borderStyle: "insert" }}
                id="mytable"
                class="table table-bordered table-striped"
              >
                <thead>
                  <tr>
                    <th>
                      <center>Kode Jadwal</center>
                    </th>
                    <th>
                      <center>Jadwal Undangan</center>
                    </th>
                    <th>
                      <center>Data</center>
                    </th>
                    <th>
                      <center>Action</center>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.rencana == "" ? (
                    <tr>
                      <td
                        style={{ color: "red", fontWeight: "bold" }}
                        colSpan="4"
                      >
                        <center>DATA TIDAK DITEMUKAN</center>
                      </td>
                    </tr>
                  ) : (
                    filteredResource.map((row, x) => (
                      <tr>
                        <td>
                          <center>{row.schedule_code}</center>
                        </td>
                        <td>
                          <center>
                            {row.schedule_date},{row.time}
                          </center>
                        </td>
                        <td>
                          <center>{row.schedule_fullname}</center>
                        </td>
                        <td>
                          <center>
                            <Link to="#">
                              <span
                                className="fa fa-search"
                                // onClick={() => {
                                //   this.viewModalHandler(row.id);
                                // }}
                                style={{
                                  fontSize: "18px",
                                  paddingRight: "30px",
                                }}
                              ></span>
                              <span
                                className="fa fa-edit"
                                // onClick={() => {
                                //   this.editModalHandler(row.id);
                                // }}
                                style={{
                                  fontSize: "18px",
                                  paddingRight: "30px",
                                }}
                              ></span>
                              <span
                                className="fa fa-trash"
                                // onClick={() => {
                                //   this.deleteModalHandler(row.id);
                                // }}
                                style={{
                                  fontSize: "18px",
                                  paddingRight: "30px",
                                }}
                              ></span>
                            </Link>
                          </center>
                        </td>
                      </tr>
                    ))
                  )}
                </tbody>
              </table>
            </Row>
            <button class="float-right" onClick={this.next}>
              Next
            </button>
            <button class="float-right" onClick={this.previous.bind(this)}>
              {" "}
              Previous{" "}
            </button>
            <Pagination bsSize="medium" items={1} activePage={1} />
          </div>
        </div>
      </div>
    );
  }
}
export default list;
