import React from "react";
import apiconfig from "../configs/api.configs.json";
import axios from "axios";
import { Link, Route, Switch } from "react-router-dom";

class Userrole extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userrole: [],
      choice: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({ choice: event.target.value });
  }

  handleSubmit(event) {
    alert("AKSES ANDA SEBAGAI : " + this.state.choice);
    event.preventDefault();
    localStorage.setItem(
      apiconfig.LS.CHOICE,
      JSON.stringify(this.state.choice).replace(/["']/g, "")
    );
    this.props.history.push("/dashboard2");
  }
  onSignOut() {
    localStorage.clear();
  }
  getListAkses() {
    var x = localStorage.getItem(apiconfig.LS.USERNAME);
    // alert(localStorage.getItem(apiconfig.LS.ID_LOGIN));
    // alert(x)
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.USERROLE,
      method: "POST",
      headers: {
        Authorization: token,
      },
      data: { abuid: x },
    };
    axios(option)
      .then((response) => {
        if (response.data.code === 200) {
          let tmp = response.data.message;
          this.setState({
            userrole: tmp,
          });
        } else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  componentDidMount() {
    this.getListAkses();
  }
  render() {
    let result = this.state.userrole.map((a) => a.abuid);

    return (
      <div>
        <div style={{ height: "4em", backgroundColor: "#000099" }}>
          <Link
            className="nav-link"
            to=""
            onClick={this.onSignOut}
            style={{ paddingTop: "1.5%", color: "#ffffff" }}
          >
            Sign out
          </Link>
        </div>
        <div
          style={{
            margin: "3% 10% 0 10%",
            height: "5em",
            backgroundColor: "#000099",
            border: "1px solid",
            padding: "3px",
            boxShadow: "5px 10px #000099",
            borderRadius: "3px",
            color: "white",
          }}
        >
          <h1 style={{ marginLeft: "4%", marginTop: "1%" }}>HI,{result[0]}</h1>
        </div>
        <div
          className="jumbotron "
          style={{
            margin: "0 10% 0 10%",
            backgroundColor: "white",
            border: "1px solid",
            padding: "3px",
            boxShadow: "5px 10px #000099",
            height: "40%",
          }}
        >
          <center style={{ marginTop: "5%" }}>
            <p
              style={{
                margin: "-3% 0 3% 0",
                fontSize: "30px",
                color: "#000099",
              }}
            >
              Selamat datang di XSIS 2.0, Silahkan pilih akses anda
            </p>
            <form onSubmit={this.handleSubmit}>
              <label>
                AKSES ROLE ANDA:
                <select value={this.state.choice} onChange={this.handleChange}>
                  <option value="">Pilih Akses Anda</option>
                  {this.state.userrole.map((row) => (
                    <option key={row.id} value={row.name}>
                      {row.name}
                    </option>
                  ))}
                </select>
              </label>
              <input className="bg-primary" type="submit" value="Pilih Akses" />
            </form>
          </center>
        </div>
      </div>
    );
  }
}
export default Userrole;
