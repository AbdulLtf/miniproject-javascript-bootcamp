import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import home from "./content/home";
import apiconfig from "../configs/api.configs.json";
import axios from "axios";

class Header2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      usermenu: []
    };
  }
  onSignOut() {
    localStorage.clear();
  }

  getListMenu() {
    var x = localStorage.getItem(apiconfig.LS.CHOICE);
    // alert(x)
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.USERMENU,
      method: "POST",
      headers: {
        Authorization: token
      },
      data: { name: x }
    };
    axios(option)
      .then(response => {
        if (response.data.code === 200) {
          let tmp = response.data.message;
          this.setState({
            usermenu: tmp
          });
          this.props.history.push("/dashboard2");
        } else {
          alert(response.data.message);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getListMenu();
  }

  render() {
    var x = localStorage.getItem(apiconfig.LS.USERNAME);
    return (
      <nav>
        <div
          className="main-header navbar navbar-expand navbar-light ml-auto"
          style={{ backgroundColor: "#000099" }}
        >
          {this.state.usermenu.map(row =>
            row.menu_level === 1 &&
            row.menu_parent === null &&
            row.menu_type === "header" ? (
              <div
                className="btn-group"
                role="group"
                aria-label="Button group with nested dropdown"
              >
                <div className="btn-group" role="group">
                  <button
                    id="btnGroupDrop1"
                    type="button"
                    className="btn dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    style={{
                      backgroundColor: "#000099",
                      color: "white",
                      marginLeft: "50px"
                    }}
                  >
                    {row.title}
                  </button>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="btnGroupDrop1"
                  >
                    {this.state.usermenu.map(rows =>
                      rows.menu_level === 2 && rows.menu_parent === row.id ? (
                        <Link
                          key={rows.id}
                          className="dropdown-item"
                          to={rows.menu_url}
                          name="choice"
                        >
                          <i className="far fa-circle nav-icon"></i>
                          <span style={{ marginLeft: "2px" }}>
                            {rows.title}
                          </span>
                        </Link>
                      ) : null
                    )}
                  </div>
                </div>
              </div>
            ) : null
          )}

          <ul className="navbar-nav ml-auto" style={{ marginRight: "4em" }}>
            <li className="nav-item dropdown">
              <a
                className="nav-link"
                data-toggle="dropdown"
                href="#"
                style={{ color: "white", fontWeight: "bold" }}
              >
                <i className="far fa-bell" style={{ marginRight: "1em" }}></i>
                <span style={{ marginRight: "1em" }}>
                  <i>Hi,{x}</i>
                </span>
                <i className="right fas fa-angle-down"></i>
              </a>
              <div className="dropdown-menu dropdown-menu dropdown-menu-right">
                <a
                  className="nav-link"
                  href="/"
                  style={{ color: "red" }}
                  onClick={this.onSignOut}
                >
                  Sign Out
                </a>
                <a
                  className="nav-link"
                  href="/userrole"
                  style={{ color: "red" }}
                >
                  Ganti Role
                </a>
                <a
                  className="nav-link"
                  href="/ubahpassword"
                  style={{ color: "red" }}
                >
                  Ganti password
                </a>
              </div>
            </li>
          </ul>
        </div>
        <div style={{ backgroundColor: "white" }}>
          <a
            style={{
              marginLeft: "3em",
              fontSize: "40px",
              fontWeight: "1000",
              color: "#000099",
              height: "20%"
            }}
          >
            XSIS 2.0
          </a>
        </div>
      </nav>
    );
  }
}

export default Header2;
