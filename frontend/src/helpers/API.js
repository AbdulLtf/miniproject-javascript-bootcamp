import axios from "axios";
import apiconfig from "../configs/api.configs.json";

const API = {
  login: async (username, password) => {
    //method login

    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.LOGIN,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        username: username,
        password: password,
      },
    };
    try {
      // try catch untuk handling error
      let result = await axios(option); // akses objek option yaitu akses url, method, headers
      // axios ialah ajaxnya ia akan menjalankan yg ada di option, brarti ia sudah menghubungin backend
      //   alert(JSON.stringify(result));
      return result.data;
    } catch (error) {
      // error apabila ada masuk ke catch

      return error;
    }
  },
};

export default API;
