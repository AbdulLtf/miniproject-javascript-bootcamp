import React from "react";
import { Switch, Route } from "react-router-dom";
import Dashboard2 from "./component/Dashboard2";
import apiconfig from "./configs/api.configs.json";
import Login from "./component/Login";
import Userrole from "./component/Userrole";
import UbahPassword from "./component/UbahPassword";
import Sidebar2 from "./component/Sidebar2";
import Header2 from "./component/Header2";
import Undangan from "./component/content/undangan/listundangan";
import Rencana from "./component/content/rencana/listrencana";

class App extends React.Component {
  render() {
    return (
      <Switch>
        <Route
          exact
          path="/"
          render={() =>
            localStorage.getItem(apiconfig.LS.TOKEN) == null ? (
              <Route exact path="/" component={Login} />
            ) : (
              <Userrole />
            )
          }
        />
        <Switch>
          <Route exact path="/userrole" component={Userrole} />
          <Route exact path="/ubahpassword" component={UbahPassword} />
          <Route path="/dashboard2" component={Dashboard2} />
          <Route path="/undangan" component={Dashboard2} />
          <Route path="/undangan" component={Header2} />
          <Route path="/undangan" component={Sidebar2} />
          <Route path="/undangan" component={Undangan} />
          <Route path="/rencana" component={Dashboard2} />
          <Route path="/rencana" component={Header2} />
          <Route path="/rencana" component={Sidebar2} />
          <Route path="/rencana" component={Rencana} />
        </Switch>
      </Switch>
    );
  }
}
export default App;
