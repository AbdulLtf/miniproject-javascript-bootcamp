const pg = require("pg");
const DatabaseConnection = require("../Config/dbp.config.json");
var DB = new pg.Pool(DatabaseConnection.config);
const bcrypt = require("bcryptjs");
const dtl_user = {
  readRoleAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }

      const query = {
        text: `select rl.id,rl.name,rl.code,adr.email,adr.abuid 
        from x_addrbook as adr 
        join x_userrole as urole 
        on adr.id = urole.addrbook_id 
        join x_role as rl 
        on rl.id = urole.role_id 
        where abuid = $1`,
        values: [docs.abuid],
      };
      client.query(query, function (err, result) {
        // console.log(query);
        // console.log(result)
        done();
        if (err) {
          data = err;
        } else {
          data = result.rows;
        }

        callback(data);
      });
    });
  },
  readMenuAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `select title,menu_icon,menu_url,menu_order,menu_level,menu_parent,menu_type,mt.id from x_menutree as mt 
                inner join x_menu_access as ma 
                on mt.id = ma.menutree_id
                inner JOIN x_role as rl 
                on rl.id = ma.role_id
                where rl.name = $1 ORDER BY mt.menu_order`,
        values: [docs.name],
      };
      client.query(query, function (err, result) {
        // console.log(query);
        // console.log(result)
        done();
        if (err) {
          data = err;
        } else {
          data = result.rows;
        }
        callback(data);
      });
    });
  },
  editPasswordAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      var salt = bcrypt.genSaltSync(10);
      let pashash = bcrypt.hashSync(docs.abpwd, salt);
      const query = {
        text: ` update x_addrbook set abpwd=$1 where abuid = $2`,
        values: [pashash, docs.abuid],
      };
      client.query(query, function (err, result) {
        // console.log(query);
        // console.log(result)
        done();
        if (err) {
          data = err;
        } else {
          data = "password berhasil diganti";
        }
        callback(data);
      });
    });
  },
  readOneUserByIdData: (callback, username) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }

      client.query(
        "SELECT * FROM x_addrbook where abuid=($1)",
        [username],
        function (err, result) {
          done();
          if (err) {
            data = err;
          } else {
            data = result.rows;
          }
          callback(data);
        }
      );
      return (uname = username);
    });
  },
  createUserAllHandlerData: (callback, docs) => {
    // untuk sementara lewat postman dulu
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      var salt = bcrypt.genSaltSync(10);
      let pashash = bcrypt.hashSync(docs.abpwd, salt);
      // console.log(pashash);
      const query = {
        text:
          "INSERT INTO x_addrbook (created_by,created_on,is_delete,is_locked,attempt,email,abuid,abpwd,fp_counter) VALUES($1,current_date,false,false,0,$2,$3,$4,0)",
        values: [docs.created_by, docs.email, docs.abuid, pashash],
      };
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil diinput";
        }
        callback(data);
      });
    });
  },
  // jika password salah maka akan mengupdate perulangan login yang salah pada kolom ul_login
  ulangUserAllHandler1: (datas) => {
    DB.connect(function (err, client, done) {
      const query = {
        text: "UPDATE x_addrbook set fp_counter=$1,attempt=$2 WHERE abuid=($3)",
        values: [datas.count, datas.attempt, datas.usern],
      };
      client.query(query);
      done();
    });
  },
  // jika ul_login sudah menacapai lebih dari tiga kali maka akan dicatat waktu dan akan di set stat-login menjadi false
  // Sehingga user tidak dapat login jika sudah melebihi batas login
  ulangUserAllHandler2: (datas) => {
    DB.connect(function (err, client, done) {
      const query = {
        text:
          "UPDATE x_addrbook set is_locked=true,attempt=$1 WHERE abuid=($2)",
        values: [datas.attempt, datas.usern],
      };
      client.query(query);
      done();
    });
  },
  // jika salah belum sampai 3 dan berhasil login, maka ulang akan di set menjadi 0 kembali
  ulangUserAllHandler3: (datas) => {
    DB.connect(function (err, client, done) {
      const query = {
        text:
          "UPDATE x_addrbook set fp_counter=0,attempt=$1,is_locked=false WHERE abuid=($2)",
        values: [datas.attempt, datas.usern],
      };
      client.query(query);
      done();
    });
  },
  ulangUserAllHandler4: () => {
    DB.connect(function (err, client, done) {
      const query = {
        text:
          "UPDATE x_addrbook set fp_counter=0,is_locked=false WHERE is_locked=true",
      };
      client.query(query);
      done();
    });
  },
  createMenuAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }

      const query = {
        text:
          "select title,url from menu join akses_menu on menu.kd_menu = akses_menu.kd_menu join user_role on akses_menu.kd_role = user_role.kd_role where akses_menu.kd_role=($1)",
        values: [docs],
      };
      client.query(query, function (err, result) {
        // console.log(query);
        // console.log(result);
        done();
        if (err) {
          data = err;
        } else {
          data = result.rows;
        }

        callback(data);
      });
    });
  },
  // updateUserAllHandlerData: (callback,docs)=>{
  //     DB.connect(function(err,client,done){
  //         var data = '';
  //         if(err){
  //             data=err;
  //         }
  //         // text:'UPDATE dosen set alamat=$1 WHERE kd_dosen=($2)',
  //         // values:[docs.alamat,docs.kd_dosen]
  //         const query ={
  //             query1:{
  //                 text:'DELETE FROM users WHERE kd_user=$1',
  //                 values:[docs.kd_user]
  //             },
  //             query2:{
  //                 text:'UPDATE users set username=$1 WHERE kd_user=($2)',
  //                 values:[docs.username,docs.kd_user]
  //             }
  //         }
  //         client.query(query.query2,function(err,result){
  //             done();
  //             if(err){
  //                 data = err;
  //             }else{
  //                 // data="data berhasil diubah dengan alamat "+query.query1.values[0]+" berdasarkan kode dosen "+query.query1.values[1]
  //                 data = "berhasil dirubah"
  //             }
  //             callback(data);
  //         })

  //     })
  // },
  // deleteUserAllHandlerData: (callback,docs)=>{
  //     DB.connect(function(err,client,done){
  //         var data = '';
  //         if(err){
  //             data=err;
  //         }
  //         console.log(docs)
  //         // text:'UPDATE dosen set alamat=$1 WHERE kd_dosen=($2)',
  //         // values:[docs.alamat,docs.kd_dosen]
  //         const query ={

  //             text:'UPDATE users set is_delete=false WHERE kd_user=($1)',
  //             values:[docs.kd_user]
  //         }
  //         client.query(query,function(err,result){
  //             done();
  //             if(err){
  //                 data = err;
  //             }else{
  //                 // data="data berhasil diubah dengan alamat "+query.query1.values[0]+" berdasarkan kode dosen "+query.query1.values[1]
  //                 data = "berhasil dirubaha"
  //             }
  //             callback(data);
  //         })

  //     })
  // }
};
module.exports = dtl_user;
