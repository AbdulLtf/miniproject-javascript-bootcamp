const pg = require("pg");
const DatabaseConnection = require("../Config/dbp.config.json");
var DB = new pg.Pool(DatabaseConnection.config);
const dtl_penjadwalan = {
  readUndanganAscAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      search = "%" + docs.search1 + "%";
      // console.log(docs.search);
      const query = {
        text: `SELECT rp.school_name,rp.major,ud.id,ud.time,ud.invitation_code,
        bio.fullname,stp.name,udd.biodata_id,ud.schedule_type_id,ud.invitation_date,
        ud.ro,ud.tro,ud.other_ro_tro,ud.location,udd.notes
        FROM x_biodata as bio
        INNER JOIN x_undangan_detail as udd
        ON bio.id=udd.biodata_id
        INNER JOIN x_undangan as ud
        ON udd.undangan_id=ud.id
        INNER JOIN x_schedule_type as stp 
        on ud.schedule_type_id = stp.id
        INNER JOIN x_riwayat_pendidikan as rp
        ON bio.id = rp.biodata_id
        WHERE ud.is_delete=false and bio.fullname like $1 or ud.is_delete=false and rp.school_name like $1 
        ORDER BY ud.id asc`,
        values: [search],
      };
      // console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          for (i = 0; i < result.rows.length; i++) {
            // console.log(result.rows[i].expired_token)
            let invitdate = result.rows[i].invitation_date.toLocaleDateString();
            invitdate = invitdate.split("/");
            invitdate = invitdate[0] + invitdate[1] + invitdate[2];
            invitdates = invitdate.split("undefined");
            result.rows[i].invitation_date = invitdates;
          }
          data = result.rows;
        }
        callback(data);
      });
    });
  },
  readUndanganDescAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      search = "%" + docs.search1 + "%";
      // console.log(docs.search);
      const query = {
        text: `SELECT rp.school_name,rp.major,ud.id,ud.time,ud.invitation_code,
        bio.fullname,stp.name,udd.biodata_id,ud.schedule_type_id,ud.invitation_date,
        ud.ro,ud.tro,ud.other_ro_tro,ud.location,udd.notes
        FROM x_biodata as bio
        INNER JOIN x_undangan_detail as udd
        ON bio.id=udd.biodata_id
        INNER JOIN x_undangan as ud
        ON udd.undangan_id=ud.id
        INNER JOIN x_schedule_type as stp 
        on ud.schedule_type_id = stp.id
        INNER JOIN x_riwayat_pendidikan as rp
        ON bio.id = rp.biodata_id
        WHERE ud.is_delete=false and bio.fullname like $1 or ud.is_delete=false and rp.school_name like $1 
        ORDER BY ud.id desc`,
        values: [search],
      };
      // console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          for (i = 0; i < result.rows.length; i++) {
            // console.log(result.rows[i].expired_token)
            let invitdate = result.rows[i].invitation_date.toLocaleDateString();
            invitdate = invitdate.split("/");
            invitdate = invitdate[0] + invitdate[1] + invitdate[2];
            invitdates = invitdate.split("undefined");
            result.rows[i].invitation_date = invitdates;
          }
          data = result.rows;
        }
        callback(data);
      });
    });
  },
  // backup read undangan
  // readUndanganAllHandlerData: (callback) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     if (err) {
  //       data = err;
  //     }
  //     client.query(
  //     `SELECT ud.id,ud.time,ud.invitation_code,
  //     bio.fullname,stp.name,ud.invitation_date,
  //     ud.ro,ud.tro,ud.other_ro_tro,ud.location,udd.notes
  // FROM x_biodata as bio
  // INNER JOIN x_undangan_detail as udd
  // ON bio.id=udd.biodata_id
  // INNER JOIN x_undangan as ud
  // ON udd.undangan_id=ud.id
  // INNER JOIN x_schedule_type as stp on ud.schedule_type_id = stp.id
  // WHERE ud.is_delete=false
  // ORDER BY ud.id`,
  //       function (err, result) {
  //         done();
  //         if (err) {
  //           data = err;
  //         } else {
  // for (i = 0; i < result.rows.length; i++) {
  //   // console.log(result.rows[i].expired_token)
  //   let invitdate = result.rows[
  //     i
  //   ].invitation_date.toLocaleDateString();
  //   invitdate = invitdate.split("/");
  //   invitdate = invitdate[0] + invitdate[1] + invitdate[2];
  //   invitdates = invitdate.split("undefined");
  //   result.rows[i].invitation_date = invitdates;
  // }
  // data = result.rows;
  //         }
  //         callback(data);
  //       }
  //     );
  //   });
  // },
  readPelamarAllHandlerData: (callback) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      client.query(
        `select xb.id,xb.fullname 
        from x_biodata as xb left 
        join x_employee as xe on xb.id=xe.biodata_id 
        where xe.biodata_id is null`,
        function (err, result) {
          done();
          if (err) {
            data = err;
          } else {
            data = result.rows;
          }
          callback(data);
        }
      );
    });
  },
  readRoTroAllHandlerData: (callback) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      client.query(
        `select xe.id,xb.fullname from x_biodata as xb inner join x_employee as xe on xb.id=xe.biodata_id `,
        function (err, result) {
          done();
          if (err) {
            data = err;
          } else {
            data = result.rows;
          }
          callback(data);
        }
      );
    });
  },
  readJenisUndanganAllHandlerData: (callback) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      client.query(`select id,name from x_schedule_type `, function (
        err,
        result
      ) {
        done();
        if (err) {
          data = err;
        } else {
          data = result.rows;
        }
        callback(data);
      });
    });
  },
  readPostRoTroAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `select xb.fullname 
        from x_biodata as xb 
        inner join x_employee as xe
        on xb.id=xe.biodata_id 
        where xb.id = $1 `,
        values: [docs.id],
      };
      // console.log(JSON.stringify(query));
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil diinput";
        }
        callback(data);
      });
    });
  },
  createUndanganAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `WITH ins AS (
               INSERT INTO x_undangan
              (created_by,created_on,is_delete,schedule_type_id,invitation_date,time,ro,tro,other_ro_tro,location)
               VALUES
              (1, current_date, false,$1, $2, $3, $4, $5,$6,$7)
              RETURNING id),

              ins2 AS (
              INSERT INTO x_undangan_detail
              (create_by,create_on,is_delete,biodata_id,notes)
              VALUES
	            (1, current_date, false, $8, $9)
              RETURNING id )

              SELECT id, 'test data'
              FROM ins;`,
        values: [
          docs.jenis_undangan,
          docs.tanggal,
          docs.jam,
          docs.ro,
          docs.tro,
          docs.rotro,
          docs.lokasi,
          docs.pelamar,
          docs.catatan,
        ],
      };
      // console.log(JSON.stringify(query));
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil diinput";
        }
        callback(data);
      });
    });
  },
  deleteUndanganAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `UPDATE x_undangan
               SET is_delete = true,
               deleted_by=$1,
               deleted_on=current_date
               WHERE
               id = $2`,
        values: [docs.deleted_by, docs.id],
      };
      // console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil di set menjadi true";
        }
        callback(data);
      });
    });
  },
  deleteUndanganDetailAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `UPDATE x_undangan_detail
        SET is_delete = true,
        delete_by=$1,
        delete_on=current_date
        WHERE
        id = $2`,
        values: [docs.deleted_by, docs.id],
      };
      // console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil di set menjadi true";
        }
        callback(data);
      });
    });
  },
  editUndanganAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      console.log(docs);
      const query = {
        text: `UPDATE x_undangan
        SET schedule_type_id=$1,
        invitation_date = $2,
        time=$3,
        ro=$4,
        tro=$5,
        other_ro_tro=$6,
        location=$7
        WHERE
        id = $8`,
        values: [
          docs.stp_id,
          docs.tanggal,
          docs.jam,
          docs.ro,
          docs.tro,
          docs.rotro,
          docs.location,
          docs.id,
        ],
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil ";
        }
        callback(data);
      });
    });
  },
  editUndanganDetailAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      console.log(docs);
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `UPDATE x_undangan_detail
        SET biodata_id = $1,
        notes=$2
        WHERE
        undangan_id = $3`,
        values: [docs.pl_id, docs.notes, docs.id],
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil";
        }
        callback(data);
      });
    });
  },
  readListRencanaAllHandlerData: (callback) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      client.query(
        `SELECT rc.id, rc.time, rc.schedule_code, 
        bio.fullname, stp.name, rc.schedule_date, 
        rc.ro, rc.tro, rc.other_ro_tro, rc.location, rc.notes
        FROM x_biodata as bio 
        INNER JOIN x_rencana_jadwal_detail as rcd 
        ON bio.id = rcd.biodata_id 
        INNER JOIN x_rencana_jadwal as rc
        ON rcd.rencana_jadwal_id = rc.id 
        INNER JOIN x_schedule_type as stp 
        on rc.schedule_type_id = stp.id 
        WHERE rc.is_delete = false 
        ORDER BY rc.id asc
        `,
        function (err, result) {
          done();
          if (err) {
            data = err;
          } else {
            data = result.rows;
          }
          callback(data);
        }
      );
    });
  },
  getListRencanaAscAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      console.log(docs);
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `SELECT rc.id,rc.time,rc.schedule_code,bio.fullname,
        stp.name,rc.schedule_date,rc.ro,rc.tro,
        rc.other_ro_tro,rc.location,rc.notes
        FROM x_biodata as bio 
        INNER JOIN x_rencana_jadwal_detail as rcd 
        ON bio.id=rcd.biodata_id 
        INNER JOIN x_rencana_jadwal as rc
        ON rcd.rencana_jadwal_id=rc.id 
        INNER JOIN x_schedule_type as stp 
        on rc.schedule_type_id = stp.id 
        WHERE rc.is_delete=false AND rc.schedule_date BETWEEN $1 AND $2
        ORDER BY rc.id asc`,
        values: [docs.search1, docs.search2],
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          for (i = 0; i < result.rows.length; i++) {
            // console.log(result.rows[i].expired_token)
            let schdate = result.rows[i].schedule_date.toLocaleDateString();
            schdate = schdate.split("/");
            schdate = schdate[0] + schdate[1] + schdate[2];
            schdates = schdate.split("undefined");
            result.rows[i].schedule_date = schdates;
          }
          data = result.rows;
        }
        callback(data);
      });
    });
  },
  getListRencanaDescAllHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      console.log(docs);
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `SELECT rc.id,rc.time,rc.schedule_code,bio.fullname,
        stp.name,rc.schedule_date,rc.ro,rc.tro,
        rc.other_ro_tro,rc.location,rc.notes
        FROM x_biodata as bio 
        INNER JOIN x_rencana_jadwal_detail as rcd 
        ON bio.id=rcd.biodata_id 
        INNER JOIN x_rencana_jadwal as rc
        ON rcd.rencana_jadwal_id=rc.id 
        INNER JOIN x_schedule_type as stp 
        on rc.schedule_type_id = stp.id 
        WHERE rc.is_delete=false AND rc.schedule_date BETWEEN $1 AND $2
        ORDER BY rc.id desc`,
        values: [docs.search1, docs.search2],
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          for (i = 0; i < result.rows.length; i++) {
            // console.log(result.rows[i].expired_token)
            let schdate = result.rows[i].schedule_date.toLocaleDateString();
            schdate = schdate.split("/");
            schdate = schdate[0] + schdate[1] + schdate[2];
            schdates = schdate.split("undefined");
            result.rows[i].schedule_date = schdates;
          }
          data = result.rows;
        }
        callback(data);
      });
    });
  },
  // readResourceAllHandlerData: (callback) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     if (err) {
  //       data = err;
  //     }
  //     client.query(
  //       `select x_resource_project.*,name
  //       from x_resource_project
  //       inner join x_client
  //       on x_resource_project.client_id = x_client.id
  //       where x_resource_project.is_delete=false`,
  //       function (err, result) {
  //         done();
  //         if (err) {
  //           data = err;
  //         } else {
  //           for (i = 0; i < result.rows.length; i++) {
  //             const options = {
  //               month: "2-digit",
  //               day: "2-digit",
  //               year: "numeric",
  //             };
  //             let exptoken = result.rows[i].start_project.toLocaleDateString(
  //               undefined,
  //               options
  //             );

  //             result.rows[i].start_project = exptoken;
  //           }
  //           for (j = 0; j < result.rows.length; j++) {
  //             const options = {
  //               month: "2-digit",
  //               day: "2-digit",
  //               year: "numeric",
  //             };
  //             let exptoken = result.rows[j].end_project.toLocaleDateString(
  //               undefined,
  //               options
  //             );

  //             result.rows[j].end_project = exptoken;
  //           }
  //           data = result.rows;
  //         }
  //         callback(data);
  //       }
  //     );
  //   });
  // },
  // readResourceDescAllHandlerData: (callback) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     if (err) {
  //       data = err;
  //     }
  //     client.query(
  //       `select x_resource_project.*,name
  //       from x_resource_project
  //       inner join x_client
  //       on x_resource_project.client_id = x_client.id
  //       where x_resource_project.is_delete=false
  //       order by name desc`,
  //       function (err, result) {
  //         done();
  //         if (err) {
  //           data = err;
  //         } else {
  //           for (i = 0; i < result.rows.length; i++) {
  //             const options = {
  //               month: "2-digit",
  //               day: "2-digit",
  //               year: "numeric",
  //             };
  //             let exptoken = result.rows[i].start_project.toLocaleDateString(
  //               undefined,
  //               options
  //             );

  //             result.rows[i].start_project = exptoken;
  //           }
  //           for (j = 0; j < result.rows.length; j++) {
  //             const options = {
  //               month: "2-digit",
  //               day: "2-digit",
  //               year: "numeric",
  //             };
  //             let exptoken = result.rows[j].end_project.toLocaleDateString(
  //               undefined,
  //               options
  //             );

  //             result.rows[j].end_project = exptoken;
  //           }
  //           data = result.rows;
  //         }
  //         callback(data);
  //       }
  //     );
  //   });
  // },
  // readResourceAscAllHandlerData: (callback) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     if (err) {
  //       data = err;
  //     }
  //     client.query(
  //       "select x_resource_project.*,name from x_resource_project inner join x_client on x_resource_project.client_id = x_client.id where x_resource_project.is_delete=false order by name asc",
  //       function (err, result) {
  //         done();
  //         if (err) {
  //           data = err;
  //         } else {
  //           for (i = 0; i < result.rows.length; i++) {
  //             const options = {
  //               month: "2-digit",
  //               day: "2-digit",
  //               year: "numeric",
  //             };
  //             let exptoken = result.rows[i].start_project.toLocaleDateString(
  //               undefined,
  //               options
  //             );

  //             result.rows[i].start_project = exptoken;
  //           }
  //           for (j = 0; j < result.rows.length; j++) {
  //             const options = {
  //               month: "2-digit",
  //               day: "2-digit",
  //               year: "numeric",
  //             };
  //             let exptoken = result.rows[j].end_project.toLocaleDateString(
  //               undefined,
  //               options
  //             );

  //             result.rows[j].end_project = exptoken;
  //           }
  //           data = result.rows;
  //         }
  //         callback(data);
  //       }
  //     );
  //   });
  // },
  // hapusListAllHandlerData: (callback, docs) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     let adrs = "";
  //     if (err) {
  //       data = err;
  //     }
  //     const query = {
  //       text:
  //         "update x_resource_project set deleted_by=($1),deleted_on=($2),is_delete=($3) where id=($4)",
  //       values: [docs.deleted_by, docs.deleted_on, docs.is_delete, docs.id],
  //     };
  //     client.query(query, function (err, result) {
  //       done();
  //       if (err) {
  //         data = err;
  //       } else {
  //         data = [(data = docs), (message = "Data Berhasil Di Update")];
  //       }

  //       callback(data);
  //     });
  //   });
  // },
  // readClientAllHandlerData: (callback) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     let adrs = "";
  //     if (err) {
  //       data = err;
  //     }

  //     client.query(
  //       "select id,name from x_client where is_delete=false",
  //       function (err, result) {
  //         done();
  //         if (err) {
  //           data = err;
  //         } else {
  //           data = result.rows;
  //         }
  //         callback(data);
  //       }
  //     );
  //   });
  // },
  // updateClientAllHandlerData: (callback, docs) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     let adrs = "";
  //     if (err) {
  //       data = err;
  //     }
  //     const query = {
  //       text:
  //         "update x_resource_project set modified_by=($1),modified_on=($2),client_id=($3),location=($4),department=($5),pic_name=($6),project_name=($7),start_project=($8),end_project=($9),project_role=($10),project_phase=($11),project_description=($12),project_technology=($13),main_task=($14) where id=($15)",
  //       values: [
  //         docs.modified_by,
  //         docs.modified_on,
  //         docs.client_id,
  //         docs.location,
  //         docs.department,
  //         docs.pic_name,
  //         docs.project_name,
  //         docs.start_project,
  //         docs.end_project,
  //         docs.project_role,
  //         docs.project_phase,
  //         docs.project_description,
  //         docs.project_technology,
  //         docs.main_task,
  //         docs.id,
  //       ],
  //     };
  //     client.query(query, function (err, result) {
  //       done();
  //       if (err) {
  //         data = err;
  //       } else {
  //         data = [(data = docs), (message = "Data Berhasil Di Update")];
  //       }

  //       callback(data);
  //     });
  //   });
  // },
  // tambahClientAllHandlerData: (callback, docs) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     let adrs = "";
  //     if (err) {
  //       data = err;
  //     }
  //     const query = {
  //       text:
  //         "insert into x_resource_project (created_by,created_on,client_id,location,department,pic_name,project_name,start_project,end_project,project_role,project_phase,project_description,project_technology,main_task,is_delete) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,false)",
  //       values: [
  //         docs.created_by,
  //         docs.created_on,
  //         docs.client_id,
  //         docs.location,
  //         docs.department,
  //         docs.pic_name,
  //         docs.project_name,
  //         docs.start_project,
  //         docs.end_project,
  //         docs.project_role,
  //         docs.project_phase,
  //         docs.project_description,
  //         docs.project_technology,
  //         docs.main_task,
  //       ],
  //     };
  //     client.query(query, function (err, result) {
  //       done();
  //       if (err) {
  //         data = err;
  //       } else {
  //         data = [(data = docs), (message = "Data Berhasil Di tambah")];
  //       }

  //       callback(data);
  //     });
  //   });
  // },
  // readResourceFindAllHandlerData: (callback, docs) => {
  //   DB.connect(function (err, client, done) {
  //     var data = "";
  //     let adrs = "";
  //     if (err) {
  //       data = err;
  //     }
  //     const query = {
  //       text:
  //         "select x_resource_project.*,name from x_resource_project inner join x_client on x_resource_project.client_id = x_client.id where x_resource_project.is_delete=false and start_project>=($1) and end_project<=($2) or x_resource_project.is_delete=false and end_project>=($1)  and start_project<=($1) or x_resource_project.is_delete=false and end_project>=($2) and start_project<=($2) or x_resource_project.is_delete=false and start_project<=($1) and end_project>=($2)",
  //       values: [docs.start_project, docs.end_project],
  //     };
  //     client.query(query, function (err, result) {
  //       done();
  //       if (err) {
  //         data = err;
  //       } else {
  //         for (i = 0; i < result.rows.length; i++) {
  //           const options = {
  //             month: "2-digit",
  //             day: "2-digit",
  //             year: "numeric",
  //           };
  //           let exptoken = result.rows[i].start_project.toLocaleDateString(
  //             undefined,
  //             options
  //           );

  //           result.rows[i].start_project = exptoken;
  //         }
  //         for (j = 0; j < result.rows.length; j++) {
  //           const options = {
  //             month: "2-digit",
  //             day: "2-digit",
  //             year: "numeric",
  //           };
  //           let exptoken = result.rows[j].end_project.toLocaleDateString(
  //             undefined,
  //             options
  //           );

  //           result.rows[j].end_project = exptoken;
  //         }
  //         data = result.rows;
  //       }
  //       callback(data);
  //     });
  //   });
  // },
};
module.exports = dtl_penjadwalan;
