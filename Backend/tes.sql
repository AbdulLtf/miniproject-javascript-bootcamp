-- insert into x_role (code,name,created_by,created_on,is_delete) values('ROLE001','ADMIN',1,CURRENT_DATE,FALSE)
-- insert into x_role (code,name,created_by,created_on,is_delete) values('ROLE002','BOOTCAMP',1,CURRENT_DATE,FALSE)
-- insert into x_role (code,name,created_by,created_on,is_delete) values('ROLE003','TRAINER',1,CURRENT_DATE,FALSE)

-- ini untuk table x_addrbook
-- insert into x_addrbook (created_by,created_on,is_delete,is_locked,attempt,email,abuid,abpwd,fp_counter) values(1,CURRENT_DATE,false,0,0,latief@gmail.com,)

-- insert into x_menutree (title,menu_order,menu_level,menu_url,menu_type,created_by,created_on,is_delete) values ('List Mahasiswa',1,1,'/listmahasiswa','navbar',1,CURRENT_DATE,false)
-- insert into x_menutree (title,menu_order,menu_level,menu_url,menu_type,created_by,created_on,is_delete) values ('List User',1,1,'/listuser','navbar',1,CURRENT_DATE,false)

-- insert into x_menu_access (menutree_id,role_id,created_by,created_on,is_delete) values (1,1,1,CURRENT_DATE,false)
-- insert into x_menu_access (menutree_id,role_id,created_by,created_on,is_delete) values (2,2,1,CURRENT_DATE,false)

--insert into x_userrole (created_by,created_on,is_delete,addrbook_id,role_id) values(1,current_date,false,2,2)

--insert into x_biodata (created_by,created_on,is_delete,fullname,nick_name,pob,dob,gender,religion_id,identity_type_id,identity_no,email,phone_number1,parent_phone_number,marital_status_id,addrbook_id,company_id) values (1,current_date,false,'User dua','User2','Jakarta','15-02-1999',true,1,2,4,'abdullatieffffz@gmail.com','0232543570','0345369869',1,null,1)

-- insert into x_undangan
--     (created_by,created_on,is_delete,schedule_type_id,invitation_date,time,ro,tro,location)
-- values
--     (1, current_date, false, 1, '28-03-2020', '13:00', 1, 2, 'Jakarta')


--insert into x_undangan_detail
--     (create_by,create_on,is_delete,undangan_id,biodata_id,notes)
-- values
--     (1, current_date, false, 2, 4, 'ini adalah catatan')

--insert into x_employee(created_by,crated_on,is_delete,biodata_id) values (1,current_date,false,1)

--insert into x_schedule_type(created_by,crated_on,is_delete,name) values (1,current_date,false,'interview')

--insert into x_udangan 
-- select title,menu_url from x_menutree as mt 
-- inner join x_menu_access as ma 
-- on mt.id = ma.menutree_id
-- inner JOIN x_role as rl 
-- on rl.id = ma.role_id
-- where rl.name = 'INTERNAL SYSTEM DEVELOPMENT'




-- ini contoh cara
-- custom autoincrement
-- CREATE SEQUENCE sequence_for_alpha_numeric
-- INCREMENT 1
--   MINVALUE 1
--   MAXVALUE 9223372036854775807
--   START 1
--   CACHE 1;

-- CREATE SEQUENCE sequence_for_schedule_code
-- INCREMENT 1
--   MINVALUE 1
--   MAXVALUE 9223372036854775807
--   START 1
--   CACHE 1;

-- CREATE TABLE table1
-- (
--     alpha_num_auto_increment_col character varying NOT NULL,
--     sample_data_col character varying,
--     CONSTRAINT table1_pkey PRIMARY KEY (alpha_num_auto_increment_col)
-- )
-- ;

-- ALTER TABLE x_undangan ALTER COLUMN invitation_code
-- SET
-- DEFAULT TO_CHAR
-- (nextval
-- ('sequence_for_alpha_numeric'::regclass),'"UD"fm000000');

-- ALTER TABLE x_rencana_jadwal ALTER COLUMN schedule_code
-- SET
-- DEFAULT TO_CHAR
-- (nextval
-- ('sequence_for_schedule_code'::regclass),'"JD"fm000000');
--------------------------------------------------------------

----- INI ADALAH CONTOH 1 QUERY MULTIPLE TABLE INSERT
-- WITH ins AS (
-- INSERT INTO x_undangan
--     (created_by,created_on,is_delete,schedule_type_id,invitation_date,time,ro,tro,location)
-- VALUES
--     (1, current_date, false, 2, '25-03-2020', '16:00', 1, 2, 'Balis')
-- RETURNING id),
-- ins2 AS
-- (
-- INSERT INTO x_undangan_detail
--     (create_by,create_on,is_delete,undangan_id,biodata_id,notes)
-- VALUES
--     (1, current_date, false, 4, 4, 'ini adalah catatans')
-- RETURNING id -- this is necessary for CTE, but not used
-- )

-- SELECT id, 'test data'
-- FROM ins;

--------------------------------------------
-- WITH inser AS (
-- INSERT INTO x_rencana_jadwal
--     (created_by,created_on,is_delete,schedule_type_id,schedule_date,time,ro,tro,location)
-- VALUES
--     (1, current_date, false, 2, '25-03-2020', '16:00', 1, 2, 'Balis')
-- RETURNING id),
-- inser2 AS
-- (
-- INSERT INTO x_rencana_jadwal_detail
--     (created_by,created_on,is_delete,rencana_jadwal_id,biodata_id)
-- VALUES
--     (1, current_date, false, 1, 4)
-- RETURNING id -- this is necessary for CTE, but not used
-- )

-- SELECT id, 'test data'
-- FROM inser;
--------------------------------------------------------



-- SELECT
--    customer_id,
--    payment_id,
--    amount,
--  payment_date
-- FROM
--    payment
-- WHERE
--    payment_date BETWEEN '2007-02-07'
-- AND '2007-02-15';