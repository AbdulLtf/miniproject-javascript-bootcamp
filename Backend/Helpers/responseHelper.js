// method sendresponse ini dibuat untuk mengambil nilai message dan statuscode yang diperoleh dari bisnis logic
module.exports = {
    sendResponse:(res, statusCode,message) => {
        let response={};
        response.message = message;
        response.code = statusCode;
        res.send(response);
    }
}