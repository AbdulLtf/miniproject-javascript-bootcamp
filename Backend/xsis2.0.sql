--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-04-10 18:40:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 308 (class 1259 OID 17863)
-- Name: sequence_for_alpha_numeric; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sequence_for_alpha_numeric
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_for_alpha_numeric OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 207 (class 1259 OID 17294)
-- Name: x_addrbook; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_addrbook (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    is_locked boolean NOT NULL,
    attempt integer NOT NULL,
    email character varying(100) NOT NULL,
    abuid character varying(50) NOT NULL,
    abpwd text NOT NULL,
    fp_token character varying(100),
    fp_expired_date date,
    fp_counter integer NOT NULL
);


ALTER TABLE public.x_addrbook OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 17292)
-- Name: x_addrbook_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_addrbook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_addrbook_id_seq OWNER TO postgres;

--
-- TOC entry 3412 (class 0 OID 0)
-- Dependencies: 206
-- Name: x_addrbook_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_addrbook_id_seq OWNED BY public.x_addrbook.id;


--
-- TOC entry 219 (class 1259 OID 17345)
-- Name: x_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_address (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    address1 character varying(1000),
    postal_code1 character varying(20),
    rt1 character varying(5),
    rw1 character varying(5),
    kelurahan1 character varying(100),
    kecamatan1 character varying(100),
    region1 character varying(100),
    address2 character varying(1000),
    postal_code2 character varying(20),
    rt2 character varying(5),
    rw2 character varying(5),
    kelurahan2 character varying(100),
    kecamatan2 character varying(100),
    region2 character varying(100)
);


ALTER TABLE public.x_address OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 17343)
-- Name: x_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_address_id_seq OWNER TO postgres;

--
-- TOC entry 3413 (class 0 OID 0)
-- Dependencies: 218
-- Name: x_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_address_id_seq OWNED BY public.x_address.id;


--
-- TOC entry 209 (class 1259 OID 17302)
-- Name: x_biodata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_biodata (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    fullname character varying(255) NOT NULL,
    nick_name character varying(100) NOT NULL,
    pob character varying(100) NOT NULL,
    dob date NOT NULL,
    gender boolean NOT NULL,
    religion_id bigint NOT NULL,
    high integer,
    weight integer,
    nationality character varying(100),
    ethnic character varying(50),
    hobby character varying(255),
    identity_type_id bigint NOT NULL,
    identity_no character varying(50) NOT NULL,
    email character varying(100) NOT NULL,
    phone_number1 character varying(50) NOT NULL,
    phone_number2 character varying(50),
    parent_phone_number character varying(50) NOT NULL,
    child_sequence character varying(5),
    how_many_brothers character varying(5),
    marital_status_id bigint NOT NULL,
    addrbook_id bigint,
    token character varying(10),
    expired_token date,
    marriage_year character varying(10),
    company_id bigint NOT NULL,
    is_process boolean,
    is_complete boolean
);


ALTER TABLE public.x_biodata OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 17477)
-- Name: x_biodata_attachment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_biodata_attachment (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    file_name character varying(100),
    file_path character varying(1000),
    notes character varying(1000),
    is_photo boolean
);


ALTER TABLE public.x_biodata_attachment OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 17475)
-- Name: x_biodata_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_biodata_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_biodata_attachment_id_seq OWNER TO postgres;

--
-- TOC entry 3414 (class 0 OID 0)
-- Dependencies: 242
-- Name: x_biodata_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_biodata_attachment_id_seq OWNED BY public.x_biodata_attachment.id;


--
-- TOC entry 208 (class 1259 OID 17300)
-- Name: x_biodata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_biodata_id_seq OWNER TO postgres;

--
-- TOC entry 3415 (class 0 OID 0)
-- Dependencies: 208
-- Name: x_biodata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_biodata_id_seq OWNED BY public.x_biodata.id;


--
-- TOC entry 245 (class 1259 OID 17488)
-- Name: x_catatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_catatan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    title character varying(100),
    note_type_id bigint,
    notes character varying(1000)
);


ALTER TABLE public.x_catatan OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 17486)
-- Name: x_catatan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_catatan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_catatan_id_seq OWNER TO postgres;

--
-- TOC entry 3416 (class 0 OID 0)
-- Dependencies: 244
-- Name: x_catatan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_catatan_id_seq OWNED BY public.x_catatan.id;


--
-- TOC entry 285 (class 1259 OID 17671)
-- Name: x_certification_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_certification_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.x_certification_type OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 17669)
-- Name: x_certification_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_certification_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_certification_type_id_seq OWNER TO postgres;

--
-- TOC entry 3417 (class 0 OID 0)
-- Dependencies: 284
-- Name: x_certification_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_certification_type_id_seq OWNED BY public.x_certification_type.id;


--
-- TOC entry 275 (class 1259 OID 17633)
-- Name: x_client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_client (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(100) NOT NULL,
    user_client_name character varying(100) NOT NULL,
    ero bigint NOT NULL,
    user_email character varying(100) NOT NULL
);


ALTER TABLE public.x_client OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 17631)
-- Name: x_client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_client_id_seq OWNER TO postgres;

--
-- TOC entry 3418 (class 0 OID 0)
-- Dependencies: 274
-- Name: x_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_client_id_seq OWNED BY public.x_client.id;


--
-- TOC entry 271 (class 1259 OID 17617)
-- Name: x_company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_company (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_company OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 17615)
-- Name: x_company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_company_id_seq OWNER TO postgres;

--
-- TOC entry 3419 (class 0 OID 0)
-- Dependencies: 270
-- Name: x_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_company_id_seq OWNED BY public.x_company.id;


--
-- TOC entry 257 (class 1259 OID 17561)
-- Name: x_education_level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_education_level (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_education_level OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 17559)
-- Name: x_education_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_education_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_education_level_id_seq OWNER TO postgres;

--
-- TOC entry 3420 (class 0 OID 0)
-- Dependencies: 256
-- Name: x_education_level_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_education_level_id_seq OWNED BY public.x_education_level.id;


--
-- TOC entry 273 (class 1259 OID 17625)
-- Name: x_employee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_employee (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    is_idle boolean,
    is_ero boolean,
    is_user_client boolean,
    ero_email character varying(100)
);


ALTER TABLE public.x_employee OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 17623)
-- Name: x_employee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_employee_id_seq OWNER TO postgres;

--
-- TOC entry 3421 (class 0 OID 0)
-- Dependencies: 272
-- Name: x_employee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_employee_id_seq OWNED BY public.x_employee.id;


--
-- TOC entry 289 (class 1259 OID 17685)
-- Name: x_employee_leave; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_employee_leave (
    id bigint NOT NULL,
    created_by bigint,
    created_on date,
    modified_by bigint NOT NULL,
    modified_on date NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on date NOT NULL,
    is_delete boolean,
    employee_id bigint,
    regular_quota integer,
    annual_collective_leave integer,
    leave_already_taken integer
);


ALTER TABLE public.x_employee_leave OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 17683)
-- Name: x_employee_leave_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_employee_leave_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_employee_leave_id_seq OWNER TO postgres;

--
-- TOC entry 3422 (class 0 OID 0)
-- Dependencies: 288
-- Name: x_employee_leave_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_employee_leave_id_seq OWNED BY public.x_employee_leave.id;


--
-- TOC entry 287 (class 1259 OID 17677)
-- Name: x_employee_training; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_employee_training (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    employee_id bigint NOT NULL,
    training_id bigint NOT NULL,
    training_organizer_id bigint,
    training_date date,
    training_type_id bigint,
    certification_type_id bigint,
    status character varying(15) NOT NULL
);


ALTER TABLE public.x_employee_training OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 17675)
-- Name: x_employee_training_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_employee_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_employee_training_id_seq OWNER TO postgres;

--
-- TOC entry 3423 (class 0 OID 0)
-- Dependencies: 286
-- Name: x_employee_training_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_employee_training_id_seq OWNED BY public.x_employee_training.id;


--
-- TOC entry 261 (class 1259 OID 17577)
-- Name: x_family_relation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_family_relation (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100),
    family_tree_type_id bigint
);


ALTER TABLE public.x_family_relation OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 17575)
-- Name: x_family_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_family_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_family_relation_id_seq OWNER TO postgres;

--
-- TOC entry 3424 (class 0 OID 0)
-- Dependencies: 260
-- Name: x_family_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_family_relation_id_seq OWNED BY public.x_family_relation.id;


--
-- TOC entry 259 (class 1259 OID 17569)
-- Name: x_family_tree_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_family_tree_type (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_family_tree_type OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 17567)
-- Name: x_family_tree_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_family_tree_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_family_tree_type_id_seq OWNER TO postgres;

--
-- TOC entry 3425 (class 0 OID 0)
-- Dependencies: 258
-- Name: x_family_tree_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_family_tree_type_id_seq OWNED BY public.x_family_tree_type.id;


--
-- TOC entry 215 (class 1259 OID 17329)
-- Name: x_identity_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_identity_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_identity_type OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 17327)
-- Name: x_identity_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_identity_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_identity_type_id_seq OWNER TO postgres;

--
-- TOC entry 3426 (class 0 OID 0)
-- Dependencies: 214
-- Name: x_identity_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_identity_type_id_seq OWNED BY public.x_identity_type.id;


--
-- TOC entry 241 (class 1259 OID 17466)
-- Name: x_keahlian; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_keahlian (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    skill_name character varying(100),
    skill_level_id bigint,
    notes character varying(1000)
);


ALTER TABLE public.x_keahlian OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 17464)
-- Name: x_keahlian_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_keahlian_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_keahlian_id_seq OWNER TO postgres;

--
-- TOC entry 3427 (class 0 OID 0)
-- Dependencies: 240
-- Name: x_keahlian_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_keahlian_id_seq OWNED BY public.x_keahlian.id;


--
-- TOC entry 239 (class 1259 OID 17455)
-- Name: x_keluarga; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_keluarga (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    family_tree_type_id bigint,
    family_relation_id bigint,
    name character varying(100),
    gender boolean NOT NULL,
    dob date,
    education_level_id bigint,
    job character varying(100),
    notes character varying(1000)
);


ALTER TABLE public.x_keluarga OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 17453)
-- Name: x_keluarga_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_keluarga_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_keluarga_id_seq OWNER TO postgres;

--
-- TOC entry 3428 (class 0 OID 0)
-- Dependencies: 238
-- Name: x_keluarga_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_keluarga_id_seq OWNED BY public.x_keluarga.id;


--
-- TOC entry 233 (class 1259 OID 17422)
-- Name: x_keterangan_tambahan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_keterangan_tambahan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    emergency_contact_name character varying(100),
    emergency_contact_phone character varying(50),
    expected_salary character varying(20),
    is_negotiable boolean,
    start_working character varying(100),
    is_ready_to_outoftown boolean,
    is_apply_other_place boolean,
    apply_place character varying(100),
    selection_phase character varying(100),
    is_ever_badly_sick boolean,
    disease_name character varying(100),
    disease_time character varying(100),
    is_ever_psychotest boolean,
    psychotest_needs character varying(100),
    psychotest_time character varying(100),
    requirementes_required character varying(500),
    other_notes character varying(1000)
);


ALTER TABLE public.x_keterangan_tambahan OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 17420)
-- Name: x_keterangan_tambahan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_keterangan_tambahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_keterangan_tambahan_id_seq OWNER TO postgres;

--
-- TOC entry 3429 (class 0 OID 0)
-- Dependencies: 232
-- Name: x_keterangan_tambahan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_keterangan_tambahan_id_seq OWNED BY public.x_keterangan_tambahan.id;


--
-- TOC entry 291 (class 1259 OID 17693)
-- Name: x_leave_name; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_leave_name (
    id bigint NOT NULL,
    created_by bigint,
    created_on date,
    modified_by bigint NOT NULL,
    modified_on date NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on date NOT NULL,
    is_delete boolean,
    leave_name_id bigint,
    s_start date,
    e_end date,
    reason character varying(255),
    leave_contact character varying(50),
    leave_address character varying(255)
);


ALTER TABLE public.x_leave_name OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 17691)
-- Name: x_leave_name_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_leave_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_leave_name_id_seq OWNER TO postgres;

--
-- TOC entry 3430 (class 0 OID 0)
-- Dependencies: 290
-- Name: x_leave_name_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_leave_name_id_seq OWNED BY public.x_leave_name.id;


--
-- TOC entry 293 (class 1259 OID 17704)
-- Name: x_leave_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_leave_request (
    id bigint NOT NULL,
    created_by bigint,
    created_on date,
    modified_by bigint NOT NULL,
    modified_on date NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on date NOT NULL,
    is_delete boolean,
    s_start date,
    e_end date,
    reason character varying(255),
    leave_contact character varying(50),
    leave_address character varying(255)
);


ALTER TABLE public.x_leave_request OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 17702)
-- Name: x_leave_request_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_leave_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_leave_request_id_seq OWNER TO postgres;

--
-- TOC entry 3431 (class 0 OID 0)
-- Dependencies: 292
-- Name: x_leave_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_leave_request_id_seq OWNED BY public.x_leave_request.id;


--
-- TOC entry 217 (class 1259 OID 17337)
-- Name: x_marital_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_marital_status (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_marital_status OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 17335)
-- Name: x_marital_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_marital_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_marital_status_id_seq OWNER TO postgres;

--
-- TOC entry 3432 (class 0 OID 0)
-- Dependencies: 216
-- Name: x_marital_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_marital_status_id_seq OWNED BY public.x_marital_status.id;


--
-- TOC entry 205 (class 1259 OID 17286)
-- Name: x_menu_access; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_menu_access (
    id bigint NOT NULL,
    menutree_id bigint NOT NULL,
    role_id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL
);


ALTER TABLE public.x_menu_access OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 17284)
-- Name: x_menu_access_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_menu_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_menu_access_id_seq OWNER TO postgres;

--
-- TOC entry 3433 (class 0 OID 0)
-- Dependencies: 204
-- Name: x_menu_access_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_menu_access_id_seq OWNED BY public.x_menu_access.id;


--
-- TOC entry 299 (class 1259 OID 17735)
-- Name: x_menutree; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_menutree (
    id bigint NOT NULL,
    title character varying(50) NOT NULL,
    menu_image_url character varying(100),
    menu_icon character varying(100),
    menu_order integer NOT NULL,
    menu_level integer NOT NULL,
    menu_parent bigint,
    menu_url character varying(100),
    menu_type character varying(10),
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL
);


ALTER TABLE public.x_menutree OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 17733)
-- Name: x_menutree_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_menutree_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_menutree_id_seq OWNER TO postgres;

--
-- TOC entry 3434 (class 0 OID 0)
-- Dependencies: 298
-- Name: x_menutree_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_menutree_id_seq OWNED BY public.x_menutree.id;


--
-- TOC entry 265 (class 1259 OID 17593)
-- Name: x_note_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_note_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_note_type OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 17591)
-- Name: x_note_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_note_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_note_type_id_seq OWNER TO postgres;

--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 264
-- Name: x_note_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_note_type_id_seq OWNED BY public.x_note_type.id;


--
-- TOC entry 247 (class 1259 OID 17499)
-- Name: x_online_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_online_test (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    period_code character varying(50),
    period integer,
    test_date date,
    expired_test date,
    user_access character varying(10),
    status character varying(50)
);


ALTER TABLE public.x_online_test OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 17507)
-- Name: x_online_test_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_online_test_detail (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    online_test_id bigint,
    test_type_id bigint,
    test_order integer
);


ALTER TABLE public.x_online_test_detail OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 17505)
-- Name: x_online_test_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_online_test_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_online_test_detail_id_seq OWNER TO postgres;

--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 248
-- Name: x_online_test_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_online_test_detail_id_seq OWNED BY public.x_online_test_detail.id;


--
-- TOC entry 246 (class 1259 OID 17497)
-- Name: x_online_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_online_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_online_test_id_seq OWNER TO postgres;

--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 246
-- Name: x_online_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_online_test_id_seq OWNED BY public.x_online_test.id;


--
-- TOC entry 237 (class 1259 OID 17444)
-- Name: x_organisasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_organisasi (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    name character varying(100),
    "position" character varying(100),
    entry_year character varying(10),
    exit_year character varying(10),
    responsibility character varying(100),
    notes character varying(1000)
);


ALTER TABLE public.x_organisasi OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 17442)
-- Name: x_organisasi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_organisasi_id_seq OWNER TO postgres;

--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 236
-- Name: x_organisasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_organisasi_id_seq OWNED BY public.x_organisasi.id;


--
-- TOC entry 231 (class 1259 OID 17411)
-- Name: x_pe_referensi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_pe_referensi (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    name character varying(100),
    "position" character varying(100),
    address_phone character varying(1000),
    relation character varying(100)
);


ALTER TABLE public.x_pe_referensi OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 17409)
-- Name: x_pe_referensi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_pe_referensi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_pe_referensi_id_seq OWNER TO postgres;

--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 230
-- Name: x_pe_referensi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_pe_referensi_id_seq OWNED BY public.x_pe_referensi.id;


--
-- TOC entry 277 (class 1259 OID 17641)
-- Name: x_placement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_placement (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    client_id bigint NOT NULL,
    employee_id bigint NOT NULL,
    is_placement_active boolean NOT NULL
);


ALTER TABLE public.x_placement OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 17639)
-- Name: x_placement_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_placement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_placement_id_seq OWNER TO postgres;

--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 276
-- Name: x_placement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_placement_id_seq OWNED BY public.x_placement.id;


--
-- TOC entry 213 (class 1259 OID 17321)
-- Name: x_religion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_religion (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_religion OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 17319)
-- Name: x_religion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_religion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_religion_id_seq OWNER TO postgres;

--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 212
-- Name: x_religion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_religion_id_seq OWNED BY public.x_religion.id;


--
-- TOC entry 251 (class 1259 OID 17515)
-- Name: x_rencana_jadwal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_rencana_jadwal (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    schedule_code character varying(20),
    schedule_date date,
    "time" character varying(10),
    ro bigint,
    tro bigint,
    schedule_type_id bigint,
    location character varying(100),
    other_ro_tro character varying(100),
    notes character varying(1000),
    is_automatic_mail boolean,
    sent_date date,
    status character varying(50)
);


ALTER TABLE public.x_rencana_jadwal OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 17526)
-- Name: x_rencana_jadwal_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_rencana_jadwal_detail (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    rencana_jadwal_id bigint NOT NULL,
    biodata_id bigint NOT NULL
);


ALTER TABLE public.x_rencana_jadwal_detail OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 17524)
-- Name: x_rencana_jadwal_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_rencana_jadwal_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_rencana_jadwal_detail_id_seq OWNER TO postgres;

--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 252
-- Name: x_rencana_jadwal_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_rencana_jadwal_detail_id_seq OWNED BY public.x_rencana_jadwal_detail.id;


--
-- TOC entry 250 (class 1259 OID 17513)
-- Name: x_rencana_jadwal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_rencana_jadwal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_rencana_jadwal_id_seq OWNER TO postgres;

--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 250
-- Name: x_rencana_jadwal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_rencana_jadwal_id_seq OWNED BY public.x_rencana_jadwal.id;


--
-- TOC entry 301 (class 1259 OID 17751)
-- Name: x_resource_project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_resource_project (
    id bigint NOT NULL,
    created_by bigint,
    created_on date,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    client_id bigint NOT NULL,
    location character varying(100),
    department character varying(100),
    pic_name character varying(100),
    project_name character varying(100) NOT NULL,
    start_project date NOT NULL,
    end_project date NOT NULL,
    project_role character varying(255) NOT NULL,
    project_phase character varying(255) NOT NULL,
    project_description character varying(255) NOT NULL,
    project_technology character varying(255) NOT NULL,
    main_task character varying(255) NOT NULL
);


ALTER TABLE public.x_resource_project OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 17749)
-- Name: x_resource_project_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_resource_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_resource_project_id_seq OWNER TO postgres;

--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 300
-- Name: x_resource_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_resource_project_id_seq OWNED BY public.x_resource_project.id;


--
-- TOC entry 221 (class 1259 OID 17356)
-- Name: x_riwayat_pekerjaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_riwayat_pekerjaan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    company_name character varying(100),
    city character varying(50),
    country character varying(50),
    join_year character varying(10),
    join_month character varying(10),
    resign_year character varying(10),
    resign_month character varying(10),
    last_position character varying(100),
    income character varying(20),
    is_it_related boolean,
    about_job character varying(1000),
    exit_reason character varying(500),
    notes character varying(5000)
);


ALTER TABLE public.x_riwayat_pekerjaan OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 17354)
-- Name: x_riwayat_pekerjaan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_riwayat_pekerjaan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_riwayat_pekerjaan_id_seq OWNER TO postgres;

--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 220
-- Name: x_riwayat_pekerjaan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_riwayat_pekerjaan_id_seq OWNED BY public.x_riwayat_pekerjaan.id;


--
-- TOC entry 227 (class 1259 OID 17389)
-- Name: x_riwayat_pelatihan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_riwayat_pelatihan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    training_name character varying(100),
    organizer character varying(50),
    training_year character varying(10),
    training_month character varying(10),
    training_duration integer,
    time_period_id bigint,
    city character varying(50),
    country character varying(50),
    notes character varying(1000)
);


ALTER TABLE public.x_riwayat_pelatihan OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 17387)
-- Name: x_riwayat_pelatihan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_riwayat_pelatihan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_riwayat_pelatihan_id_seq OWNER TO postgres;

--
-- TOC entry 3446 (class 0 OID 0)
-- Dependencies: 226
-- Name: x_riwayat_pelatihan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_riwayat_pelatihan_id_seq OWNED BY public.x_riwayat_pelatihan.id;


--
-- TOC entry 225 (class 1259 OID 17378)
-- Name: x_riwayat_pendidikan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_riwayat_pendidikan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    school_name character varying(100),
    city character varying(50),
    country character varying(50),
    education_level_id bigint,
    entry_year character varying(10),
    graduation_year character varying(10),
    major character varying(1000),
    gpa double precision,
    notes character varying(1000),
    orders integer,
    judul_ta character varying(255),
    deskripsi_ta character varying(5000)
);


ALTER TABLE public.x_riwayat_pendidikan OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 17376)
-- Name: x_riwayat_pendidikan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_riwayat_pendidikan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_riwayat_pendidikan_id_seq OWNER TO postgres;

--
-- TOC entry 3447 (class 0 OID 0)
-- Dependencies: 224
-- Name: x_riwayat_pendidikan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_riwayat_pendidikan_id_seq OWNED BY public.x_riwayat_pendidikan.id;


--
-- TOC entry 223 (class 1259 OID 17367)
-- Name: x_riwayat_proyek; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_riwayat_proyek (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    riwayat_pekerjaan_id bigint NOT NULL,
    start_year character varying(10),
    start_month character varying(10),
    project_name character varying(50),
    project_duration integer,
    time_period_id bigint,
    client character varying(100),
    project_position character varying(100),
    description character varying(1000)
);


ALTER TABLE public.x_riwayat_proyek OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 17365)
-- Name: x_riwayat_proyek_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_riwayat_proyek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_riwayat_proyek_id_seq OWNER TO postgres;

--
-- TOC entry 3448 (class 0 OID 0)
-- Dependencies: 222
-- Name: x_riwayat_proyek_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_riwayat_proyek_id_seq OWNED BY public.x_riwayat_proyek.id;


--
-- TOC entry 203 (class 1259 OID 17270)
-- Name: x_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_role (
    id bigint NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL
);


ALTER TABLE public.x_role OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17268)
-- Name: x_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_role_id_seq OWNER TO postgres;

--
-- TOC entry 3449 (class 0 OID 0)
-- Dependencies: 202
-- Name: x_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_role_id_seq OWNED BY public.x_role.id;


--
-- TOC entry 269 (class 1259 OID 17609)
-- Name: x_schedule_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_schedule_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_schedule_type OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 17607)
-- Name: x_schedule_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_schedule_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_schedule_type_id_seq OWNER TO postgres;

--
-- TOC entry 3450 (class 0 OID 0)
-- Dependencies: 268
-- Name: x_schedule_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_schedule_type_id_seq OWNED BY public.x_schedule_type.id;


--
-- TOC entry 229 (class 1259 OID 17400)
-- Name: x_sertifikasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_sertifikasi (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    certificate_name character varying(200),
    publisher character varying(100),
    valid_start_year character varying(10),
    valid_start_month character varying(10),
    until_year character varying(10),
    until_month character varying(10),
    notes character varying(1000)
);


ALTER TABLE public.x_sertifikasi OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 17398)
-- Name: x_sertifikasi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_sertifikasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_sertifikasi_id_seq OWNER TO postgres;

--
-- TOC entry 3451 (class 0 OID 0)
-- Dependencies: 228
-- Name: x_sertifikasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_sertifikasi_id_seq OWNED BY public.x_sertifikasi.id;


--
-- TOC entry 263 (class 1259 OID 17585)
-- Name: x_skill_level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_skill_level (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_skill_level OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 17583)
-- Name: x_skill_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_skill_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_skill_level_id_seq OWNER TO postgres;

--
-- TOC entry 3452 (class 0 OID 0)
-- Dependencies: 262
-- Name: x_skill_level_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_skill_level_id_seq OWNED BY public.x_skill_level.id;


--
-- TOC entry 235 (class 1259 OID 17433)
-- Name: x_sumber_loker; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_sumber_loker (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    vacancy_source character varying(20),
    candidate_type character varying(10),
    event_name character varying(100),
    career_center_name character varying(100),
    referrer_name character varying(100),
    referrer_phone character varying(20),
    referrer_email character varying(100),
    other_source character varying(100),
    last_income character varying(20),
    apply_date date
);


ALTER TABLE public.x_sumber_loker OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 17431)
-- Name: x_sumber_loker_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_sumber_loker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_sumber_loker_id_seq OWNER TO postgres;

--
-- TOC entry 3453 (class 0 OID 0)
-- Dependencies: 234
-- Name: x_sumber_loker_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_sumber_loker_id_seq OWNED BY public.x_sumber_loker.id;


--
-- TOC entry 267 (class 1259 OID 17601)
-- Name: x_test_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_test_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_test_type OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 17599)
-- Name: x_test_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_test_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_test_type_id_seq OWNER TO postgres;

--
-- TOC entry 3454 (class 0 OID 0)
-- Dependencies: 266
-- Name: x_test_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_test_type_id_seq OWNED BY public.x_test_type.id;


--
-- TOC entry 255 (class 1259 OID 17553)
-- Name: x_time_period; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_time_period (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);


ALTER TABLE public.x_time_period OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 17551)
-- Name: x_time_period_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_time_period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_time_period_id_seq OWNER TO postgres;

--
-- TOC entry 3455 (class 0 OID 0)
-- Dependencies: 254
-- Name: x_time_period_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_time_period_id_seq OWNED BY public.x_time_period.id;


--
-- TOC entry 295 (class 1259 OID 17715)
-- Name: x_timesheet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_timesheet (
    id bigint NOT NULL,
    created_by bigint,
    created_on date,
    modified_by bigint NOT NULL,
    modified_on date NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on date NOT NULL,
    is_delete boolean,
    status character varying(15),
    placement_id bigint,
    timesheet_date date,
    s_start character varying(5),
    e_end character varying(5),
    overtime boolean NOT NULL,
    start_ot character varying(5) NOT NULL,
    end_ot character varying(5) NOT NULL,
    activity character varying(255),
    user_approval character varying(50),
    submitted_on date,
    approved_on date,
    ero_status character varying(50),
    sent_on date,
    collected_on date
);


ALTER TABLE public.x_timesheet OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 17723)
-- Name: x_timesheet_assessment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_timesheet_assessment (
    id bigint NOT NULL,
    created_by bigint,
    created_on date,
    modified_by bigint NOT NULL,
    modified_on date NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on date NOT NULL,
    is_delete boolean,
    year integer,
    month integer,
    placement_id bigint,
    target_result integer,
    competency integer,
    discipline integer
);


ALTER TABLE public.x_timesheet_assessment OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 17721)
-- Name: x_timesheet_assessment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_timesheet_assessment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_timesheet_assessment_id_seq OWNER TO postgres;

--
-- TOC entry 3456 (class 0 OID 0)
-- Dependencies: 296
-- Name: x_timesheet_assessment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_timesheet_assessment_id_seq OWNED BY public.x_timesheet_assessment.id;


--
-- TOC entry 294 (class 1259 OID 17713)
-- Name: x_timesheet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_timesheet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_timesheet_id_seq OWNER TO postgres;

--
-- TOC entry 3457 (class 0 OID 0)
-- Dependencies: 294
-- Name: x_timesheet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_timesheet_id_seq OWNED BY public.x_timesheet.id;


--
-- TOC entry 279 (class 1259 OID 17649)
-- Name: x_training; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_training (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.x_training OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 17647)
-- Name: x_training_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_training_id_seq OWNER TO postgres;

--
-- TOC entry 3458 (class 0 OID 0)
-- Dependencies: 278
-- Name: x_training_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_training_id_seq OWNED BY public.x_training.id;


--
-- TOC entry 281 (class 1259 OID 17657)
-- Name: x_training_organizer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_training_organizer (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(100) NOT NULL,
    notes character varying(100) NOT NULL
);


ALTER TABLE public.x_training_organizer OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 17655)
-- Name: x_training_organizer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_training_organizer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_training_organizer_id_seq OWNER TO postgres;

--
-- TOC entry 3459 (class 0 OID 0)
-- Dependencies: 280
-- Name: x_training_organizer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_training_organizer_id_seq OWNED BY public.x_training_organizer.id;


--
-- TOC entry 283 (class 1259 OID 17665)
-- Name: x_training_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_training_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.x_training_type OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 17663)
-- Name: x_training_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_training_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_training_type_id_seq OWNER TO postgres;

--
-- TOC entry 3460 (class 0 OID 0)
-- Dependencies: 282
-- Name: x_training_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_training_type_id_seq OWNED BY public.x_training_type.id;


--
-- TOC entry 304 (class 1259 OID 17843)
-- Name: x_undangan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_undangan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    schedule_type_id bigint,
    invitation_date date,
    invitation_code character varying(20) DEFAULT to_char(nextval('public.sequence_for_alpha_numeric'::regclass), '"UD"fm000000'::text),
    "time" character varying(10),
    ro bigint,
    tro bigint,
    other_ro_tro character varying(100),
    location character varying(100),
    status character varying(50)
);


ALTER TABLE public.x_undangan OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 17853)
-- Name: x_undangan_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_undangan_detail (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    undangan_id integer NOT NULL,
    biodata_id bigint NOT NULL,
    notes character varying(1000)
);


ALTER TABLE public.x_undangan_detail OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 17849)
-- Name: x_undangan_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_undangan_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_undangan_detail_id_seq OWNER TO postgres;

--
-- TOC entry 3461 (class 0 OID 0)
-- Dependencies: 305
-- Name: x_undangan_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_undangan_detail_id_seq OWNED BY public.x_undangan_detail.id;


--
-- TOC entry 306 (class 1259 OID 17851)
-- Name: x_undangan_detail_undangan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_undangan_detail_undangan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_undangan_detail_undangan_id_seq OWNER TO postgres;

--
-- TOC entry 3462 (class 0 OID 0)
-- Dependencies: 306
-- Name: x_undangan_detail_undangan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_undangan_detail_undangan_id_seq OWNED BY public.x_undangan_detail.undangan_id;


--
-- TOC entry 303 (class 1259 OID 17841)
-- Name: x_undangan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_undangan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_undangan_id_seq OWNER TO postgres;

--
-- TOC entry 3463 (class 0 OID 0)
-- Dependencies: 303
-- Name: x_undangan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_undangan_id_seq OWNED BY public.x_undangan.id;


--
-- TOC entry 211 (class 1259 OID 17313)
-- Name: x_userrole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_userrole (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    addrbook_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.x_userrole OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 17311)
-- Name: x_userrole_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.x_userrole_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_userrole_id_seq OWNER TO postgres;

--
-- TOC entry 3464 (class 0 OID 0)
-- Dependencies: 210
-- Name: x_userrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.x_userrole_id_seq OWNED BY public.x_userrole.id;


--
-- TOC entry 302 (class 1259 OID 17760)
-- Name: xxx; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.xxx
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xxx OWNER TO postgres;

--
-- TOC entry 3022 (class 2604 OID 17297)
-- Name: x_addrbook id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_addrbook ALTER COLUMN id SET DEFAULT nextval('public.x_addrbook_id_seq'::regclass);


--
-- TOC entry 3028 (class 2604 OID 17348)
-- Name: x_address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_address ALTER COLUMN id SET DEFAULT nextval('public.x_address_id_seq'::regclass);


--
-- TOC entry 3023 (class 2604 OID 17305)
-- Name: x_biodata id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_biodata ALTER COLUMN id SET DEFAULT nextval('public.x_biodata_id_seq'::regclass);


--
-- TOC entry 3040 (class 2604 OID 17480)
-- Name: x_biodata_attachment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_biodata_attachment ALTER COLUMN id SET DEFAULT nextval('public.x_biodata_attachment_id_seq'::regclass);


--
-- TOC entry 3041 (class 2604 OID 17491)
-- Name: x_catatan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_catatan ALTER COLUMN id SET DEFAULT nextval('public.x_catatan_id_seq'::regclass);


--
-- TOC entry 3061 (class 2604 OID 17674)
-- Name: x_certification_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_certification_type ALTER COLUMN id SET DEFAULT nextval('public.x_certification_type_id_seq'::regclass);


--
-- TOC entry 3056 (class 2604 OID 17636)
-- Name: x_client id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_client ALTER COLUMN id SET DEFAULT nextval('public.x_client_id_seq'::regclass);


--
-- TOC entry 3054 (class 2604 OID 17620)
-- Name: x_company id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_company ALTER COLUMN id SET DEFAULT nextval('public.x_company_id_seq'::regclass);


--
-- TOC entry 3047 (class 2604 OID 17564)
-- Name: x_education_level id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_education_level ALTER COLUMN id SET DEFAULT nextval('public.x_education_level_id_seq'::regclass);


--
-- TOC entry 3055 (class 2604 OID 17628)
-- Name: x_employee id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_employee ALTER COLUMN id SET DEFAULT nextval('public.x_employee_id_seq'::regclass);


--
-- TOC entry 3063 (class 2604 OID 17688)
-- Name: x_employee_leave id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_employee_leave ALTER COLUMN id SET DEFAULT nextval('public.x_employee_leave_id_seq'::regclass);


--
-- TOC entry 3062 (class 2604 OID 17680)
-- Name: x_employee_training id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_employee_training ALTER COLUMN id SET DEFAULT nextval('public.x_employee_training_id_seq'::regclass);


--
-- TOC entry 3049 (class 2604 OID 17580)
-- Name: x_family_relation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_family_relation ALTER COLUMN id SET DEFAULT nextval('public.x_family_relation_id_seq'::regclass);


--
-- TOC entry 3048 (class 2604 OID 17572)
-- Name: x_family_tree_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_family_tree_type ALTER COLUMN id SET DEFAULT nextval('public.x_family_tree_type_id_seq'::regclass);


--
-- TOC entry 3026 (class 2604 OID 17332)
-- Name: x_identity_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_identity_type ALTER COLUMN id SET DEFAULT nextval('public.x_identity_type_id_seq'::regclass);


--
-- TOC entry 3039 (class 2604 OID 17469)
-- Name: x_keahlian id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_keahlian ALTER COLUMN id SET DEFAULT nextval('public.x_keahlian_id_seq'::regclass);


--
-- TOC entry 3038 (class 2604 OID 17458)
-- Name: x_keluarga id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_keluarga ALTER COLUMN id SET DEFAULT nextval('public.x_keluarga_id_seq'::regclass);


--
-- TOC entry 3035 (class 2604 OID 17425)
-- Name: x_keterangan_tambahan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_keterangan_tambahan ALTER COLUMN id SET DEFAULT nextval('public.x_keterangan_tambahan_id_seq'::regclass);


--
-- TOC entry 3064 (class 2604 OID 17696)
-- Name: x_leave_name id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_leave_name ALTER COLUMN id SET DEFAULT nextval('public.x_leave_name_id_seq'::regclass);


--
-- TOC entry 3065 (class 2604 OID 17707)
-- Name: x_leave_request id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_leave_request ALTER COLUMN id SET DEFAULT nextval('public.x_leave_request_id_seq'::regclass);


--
-- TOC entry 3027 (class 2604 OID 17340)
-- Name: x_marital_status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_marital_status ALTER COLUMN id SET DEFAULT nextval('public.x_marital_status_id_seq'::regclass);


--
-- TOC entry 3021 (class 2604 OID 17289)
-- Name: x_menu_access id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_menu_access ALTER COLUMN id SET DEFAULT nextval('public.x_menu_access_id_seq'::regclass);


--
-- TOC entry 3068 (class 2604 OID 17738)
-- Name: x_menutree id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_menutree ALTER COLUMN id SET DEFAULT nextval('public.x_menutree_id_seq'::regclass);


--
-- TOC entry 3051 (class 2604 OID 17596)
-- Name: x_note_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_note_type ALTER COLUMN id SET DEFAULT nextval('public.x_note_type_id_seq'::regclass);


--
-- TOC entry 3042 (class 2604 OID 17502)
-- Name: x_online_test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_online_test ALTER COLUMN id SET DEFAULT nextval('public.x_online_test_id_seq'::regclass);


--
-- TOC entry 3043 (class 2604 OID 17510)
-- Name: x_online_test_detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_online_test_detail ALTER COLUMN id SET DEFAULT nextval('public.x_online_test_detail_id_seq'::regclass);


--
-- TOC entry 3037 (class 2604 OID 17447)
-- Name: x_organisasi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_organisasi ALTER COLUMN id SET DEFAULT nextval('public.x_organisasi_id_seq'::regclass);


--
-- TOC entry 3034 (class 2604 OID 17414)
-- Name: x_pe_referensi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_pe_referensi ALTER COLUMN id SET DEFAULT nextval('public.x_pe_referensi_id_seq'::regclass);


--
-- TOC entry 3057 (class 2604 OID 17644)
-- Name: x_placement id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_placement ALTER COLUMN id SET DEFAULT nextval('public.x_placement_id_seq'::regclass);


--
-- TOC entry 3025 (class 2604 OID 17324)
-- Name: x_religion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_religion ALTER COLUMN id SET DEFAULT nextval('public.x_religion_id_seq'::regclass);


--
-- TOC entry 3044 (class 2604 OID 17518)
-- Name: x_rencana_jadwal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_rencana_jadwal ALTER COLUMN id SET DEFAULT nextval('public.x_rencana_jadwal_id_seq'::regclass);


--
-- TOC entry 3045 (class 2604 OID 17529)
-- Name: x_rencana_jadwal_detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_rencana_jadwal_detail ALTER COLUMN id SET DEFAULT nextval('public.x_rencana_jadwal_detail_id_seq'::regclass);


--
-- TOC entry 3069 (class 2604 OID 17754)
-- Name: x_resource_project id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_resource_project ALTER COLUMN id SET DEFAULT nextval('public.x_resource_project_id_seq'::regclass);


--
-- TOC entry 3029 (class 2604 OID 17359)
-- Name: x_riwayat_pekerjaan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_pekerjaan ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_pekerjaan_id_seq'::regclass);


--
-- TOC entry 3032 (class 2604 OID 17392)
-- Name: x_riwayat_pelatihan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_pelatihan ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_pelatihan_id_seq'::regclass);


--
-- TOC entry 3031 (class 2604 OID 17381)
-- Name: x_riwayat_pendidikan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_pendidikan ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_pendidikan_id_seq'::regclass);


--
-- TOC entry 3030 (class 2604 OID 17370)
-- Name: x_riwayat_proyek id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_proyek ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_proyek_id_seq'::regclass);


--
-- TOC entry 3020 (class 2604 OID 17273)
-- Name: x_role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_role ALTER COLUMN id SET DEFAULT nextval('public.x_role_id_seq'::regclass);


--
-- TOC entry 3053 (class 2604 OID 17612)
-- Name: x_schedule_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_schedule_type ALTER COLUMN id SET DEFAULT nextval('public.x_schedule_type_id_seq'::regclass);


--
-- TOC entry 3033 (class 2604 OID 17403)
-- Name: x_sertifikasi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_sertifikasi ALTER COLUMN id SET DEFAULT nextval('public.x_sertifikasi_id_seq'::regclass);


--
-- TOC entry 3050 (class 2604 OID 17588)
-- Name: x_skill_level id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_skill_level ALTER COLUMN id SET DEFAULT nextval('public.x_skill_level_id_seq'::regclass);


--
-- TOC entry 3036 (class 2604 OID 17436)
-- Name: x_sumber_loker id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_sumber_loker ALTER COLUMN id SET DEFAULT nextval('public.x_sumber_loker_id_seq'::regclass);


--
-- TOC entry 3052 (class 2604 OID 17604)
-- Name: x_test_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_test_type ALTER COLUMN id SET DEFAULT nextval('public.x_test_type_id_seq'::regclass);


--
-- TOC entry 3046 (class 2604 OID 17556)
-- Name: x_time_period id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_time_period ALTER COLUMN id SET DEFAULT nextval('public.x_time_period_id_seq'::regclass);


--
-- TOC entry 3066 (class 2604 OID 17718)
-- Name: x_timesheet id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_timesheet ALTER COLUMN id SET DEFAULT nextval('public.x_timesheet_id_seq'::regclass);


--
-- TOC entry 3067 (class 2604 OID 17726)
-- Name: x_timesheet_assessment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_timesheet_assessment ALTER COLUMN id SET DEFAULT nextval('public.x_timesheet_assessment_id_seq'::regclass);


--
-- TOC entry 3058 (class 2604 OID 17652)
-- Name: x_training id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training ALTER COLUMN id SET DEFAULT nextval('public.x_training_id_seq'::regclass);


--
-- TOC entry 3059 (class 2604 OID 17660)
-- Name: x_training_organizer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training_organizer ALTER COLUMN id SET DEFAULT nextval('public.x_training_organizer_id_seq'::regclass);


--
-- TOC entry 3060 (class 2604 OID 17668)
-- Name: x_training_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training_type ALTER COLUMN id SET DEFAULT nextval('public.x_training_type_id_seq'::regclass);


--
-- TOC entry 3070 (class 2604 OID 17846)
-- Name: x_undangan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_undangan ALTER COLUMN id SET DEFAULT nextval('public.x_undangan_id_seq'::regclass);


--
-- TOC entry 3072 (class 2604 OID 17856)
-- Name: x_undangan_detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_undangan_detail ALTER COLUMN id SET DEFAULT nextval('public.x_undangan_detail_id_seq'::regclass);


--
-- TOC entry 3073 (class 2604 OID 17857)
-- Name: x_undangan_detail undangan_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_undangan_detail ALTER COLUMN undangan_id SET DEFAULT nextval('public.x_undangan_detail_undangan_id_seq'::regclass);


--
-- TOC entry 3024 (class 2604 OID 17316)
-- Name: x_userrole id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_userrole ALTER COLUMN id SET DEFAULT nextval('public.x_userrole_id_seq'::regclass);


--
-- TOC entry 3305 (class 0 OID 17294)
-- Dependencies: 207
-- Data for Name: x_addrbook; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_addrbook (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, is_locked, attempt, email, abuid, abpwd, fp_token, fp_expired_date, fp_counter) FROM stdin;
2	1	2020-03-24	\N	\N	\N	\N	f	f	5	latif@gmail.com	latif	$2a$10$XolO7851YKI1/qgqBfPLLuIA80UIlNDCvc32BEbnl1jqTG.bcWSCe	\N	\N	0
1	1	2020-03-19	\N	\N	\N	\N	f	f	353	abdul@gmail.com	abdul	$2a$10$YUBieELKwVwokZ10yoc0suauJbwOzS5GuBZBINnywEWB7BmJDFQly	\N	\N	0
\.


--
-- TOC entry 3317 (class 0 OID 17345)
-- Dependencies: 219
-- Data for Name: x_address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_address (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, address1, postal_code1, rt1, rw1, kelurahan1, kecamatan1, region1, address2, postal_code2, rt2, rw2, kelurahan2, kecamatan2, region2) FROM stdin;
\.


--
-- TOC entry 3307 (class 0 OID 17302)
-- Dependencies: 209
-- Data for Name: x_biodata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_biodata (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, fullname, nick_name, pob, dob, gender, religion_id, high, weight, nationality, ethnic, hobby, identity_type_id, identity_no, email, phone_number1, phone_number2, parent_phone_number, child_sequence, how_many_brothers, marital_status_id, addrbook_id, token, expired_token, marriage_year, company_id, is_process, is_complete) FROM stdin;
1	1	2020-03-27	\N	\N	\N	\N	f	Abdul Latief	Latief	Jakarta	1997-02-15	t	1	\N	\N	\N	\N	\N	1	1	abdullatief@gmail.com	0232543567	\N	0345369864	\N	\N	1	1	\N	\N	\N	1	\N	\N
2	1	2020-03-27	\N	\N	\N	\N	f	Latief Abdul 	Abdul	Jakarta	1997-02-15	t	2	\N	\N	\N	\N	\N	2	2	abdullatief@gmail.com	0232543567	\N	0345369864	\N	\N	1	2	\N	\N	\N	1	\N	\N
3	1	2020-03-27	\N	\N	\N	\N	f	User satu 	User1	Jakarta	1998-02-15	t	1	\N	\N	\N	\N	\N	2	3	abdullatieffff@gmail.com	0232543569	\N	0345369865	\N	\N	1	\N	\N	\N	\N	1	\N	\N
4	1	2020-03-27	\N	\N	\N	\N	f	User dua	User2	Jakarta	1999-02-15	t	1	\N	\N	\N	\N	\N	2	4	abdullatieffffz@gmail.com	0232543570	\N	0345369869	\N	\N	1	\N	\N	\N	\N	1	\N	\N
\.


--
-- TOC entry 3341 (class 0 OID 17477)
-- Dependencies: 243
-- Data for Name: x_biodata_attachment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_biodata_attachment (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, file_name, file_path, notes, is_photo) FROM stdin;
\.


--
-- TOC entry 3343 (class 0 OID 17488)
-- Dependencies: 245
-- Data for Name: x_catatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_catatan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, title, note_type_id, notes) FROM stdin;
\.


--
-- TOC entry 3383 (class 0 OID 17671)
-- Dependencies: 285
-- Data for Name: x_certification_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_certification_type (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, code, name) FROM stdin;
\.


--
-- TOC entry 3373 (class 0 OID 17633)
-- Dependencies: 275
-- Data for Name: x_client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_client (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, user_client_name, ero, user_email) FROM stdin;
\.


--
-- TOC entry 3369 (class 0 OID 17617)
-- Dependencies: 271
-- Data for Name: x_company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_company (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3355 (class 0 OID 17561)
-- Dependencies: 257
-- Data for Name: x_education_level; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_education_level (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3371 (class 0 OID 17625)
-- Dependencies: 273
-- Data for Name: x_employee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_employee (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, is_idle, is_ero, is_user_client, ero_email) FROM stdin;
1	1	2020-03-27	\N	\N	\N	\N	f	1	\N	\N	\N	\N
2	1	2020-03-27	\N	\N	\N	\N	f	2	\N	\N	\N	\N
\.


--
-- TOC entry 3387 (class 0 OID 17685)
-- Dependencies: 289
-- Data for Name: x_employee_leave; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_employee_leave (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, employee_id, regular_quota, annual_collective_leave, leave_already_taken) FROM stdin;
\.


--
-- TOC entry 3385 (class 0 OID 17677)
-- Dependencies: 287
-- Data for Name: x_employee_training; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_employee_training (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, employee_id, training_id, training_organizer_id, training_date, training_type_id, certification_type_id, status) FROM stdin;
\.


--
-- TOC entry 3359 (class 0 OID 17577)
-- Dependencies: 261
-- Data for Name: x_family_relation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_family_relation (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description, family_tree_type_id) FROM stdin;
\.


--
-- TOC entry 3357 (class 0 OID 17569)
-- Dependencies: 259
-- Data for Name: x_family_tree_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_family_tree_type (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3313 (class 0 OID 17329)
-- Dependencies: 215
-- Data for Name: x_identity_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_identity_type (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3339 (class 0 OID 17466)
-- Dependencies: 241
-- Data for Name: x_keahlian; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_keahlian (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, skill_name, skill_level_id, notes) FROM stdin;
\.


--
-- TOC entry 3337 (class 0 OID 17455)
-- Dependencies: 239
-- Data for Name: x_keluarga; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_keluarga (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, family_tree_type_id, family_relation_id, name, gender, dob, education_level_id, job, notes) FROM stdin;
\.


--
-- TOC entry 3331 (class 0 OID 17422)
-- Dependencies: 233
-- Data for Name: x_keterangan_tambahan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_keterangan_tambahan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, emergency_contact_name, emergency_contact_phone, expected_salary, is_negotiable, start_working, is_ready_to_outoftown, is_apply_other_place, apply_place, selection_phase, is_ever_badly_sick, disease_name, disease_time, is_ever_psychotest, psychotest_needs, psychotest_time, requirementes_required, other_notes) FROM stdin;
\.


--
-- TOC entry 3389 (class 0 OID 17693)
-- Dependencies: 291
-- Data for Name: x_leave_name; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_leave_name (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, leave_name_id, s_start, e_end, reason, leave_contact, leave_address) FROM stdin;
\.


--
-- TOC entry 3391 (class 0 OID 17704)
-- Dependencies: 293
-- Data for Name: x_leave_request; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_leave_request (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, s_start, e_end, reason, leave_contact, leave_address) FROM stdin;
\.


--
-- TOC entry 3315 (class 0 OID 17337)
-- Dependencies: 217
-- Data for Name: x_marital_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_marital_status (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3303 (class 0 OID 17286)
-- Dependencies: 205
-- Data for Name: x_menu_access; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_menu_access (id, menutree_id, role_id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
3	2	1	1	2020-03-23	\N	\N	\N	\N	f
4	3	1	1	2020-03-26	\N	\N	\N	\N	f
5	4	1	1	2020-03-26	\N	\N	\N	\N	f
6	5	1	1	2020-03-26	\N	\N	\N	\N	f
2	1	2	1	2020-03-23	\N	\N	\N	\N	f
7	5	2	1	2020-03-26	\N	\N	\N	\N	f
8	6	1	1	2020-03-31	\N	\N	\N	\N	f
9	6	2	1	2020-03-31	\N	\N	\N	\N	f
10	7	1	1	2020-03-31	\N	\N	\N	\N	f
1	1	1	1	2020-03-31	\N	\N	\N	\N	f
13	8	1	1	2020-03-31	\N	\N	\N	\N	f
14	9	1	1	2020-03-31	\N	\N	\N	\N	f
15	9	2	1	2020-03-31	\N	\N	\N	\N	f
\.


--
-- TOC entry 3397 (class 0 OID 17735)
-- Dependencies: 299
-- Data for Name: x_menutree; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_menutree (id, title, menu_image_url, menu_icon, menu_order, menu_level, menu_parent, menu_url, menu_type, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
8	Header Submenu2	\N	\N	2	2	7	/headersubmenu2	header	1	2020-03-31	\N	\N	\N	\N	f
9	Header Submenu	\N	\N	2	2	6	/headersubmenu	header	1	2020-03-31	\N	\N	\N	\N	f
7	Header Menu2	\N	\N	1	1	\N	\N	header	1	2020-03-31	\N	\N	\N	\N	f
6	Header Menu	\N	\N	1	1	\N	\N	header	1	2020-03-31	\N	\N	\N	\N	f
1	Beranda	\N	parent	2	1	\N	\N	navbar	1	2020-03-23	\N	\N	\N	\N	f
2	Penjadwalan	\N	parent	1	1	\N	\N	navbar	1	2020-03-23	\N	\N	\N	\N	f
5	Home	\N	child	1	2	1	/dashboard2	navbar	1	2020-03-26	\N	\N	\N	\N	f
3	Rencana	\N	child	2	2	2	/rencana	navbar	1	2020-03-26	\N	\N	\N	\N	f
4	Undangan	\N	child	1	2	2	/undangan	navbar	1	2020-03-26	\N	\N	\N	\N	f
\.


--
-- TOC entry 3363 (class 0 OID 17593)
-- Dependencies: 265
-- Data for Name: x_note_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_note_type (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3345 (class 0 OID 17499)
-- Dependencies: 247
-- Data for Name: x_online_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_online_test (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, period_code, period, test_date, expired_test, user_access, status) FROM stdin;
\.


--
-- TOC entry 3347 (class 0 OID 17507)
-- Dependencies: 249
-- Data for Name: x_online_test_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_online_test_detail (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, online_test_id, test_type_id, test_order) FROM stdin;
\.


--
-- TOC entry 3335 (class 0 OID 17444)
-- Dependencies: 237
-- Data for Name: x_organisasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_organisasi (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, name, "position", entry_year, exit_year, responsibility, notes) FROM stdin;
\.


--
-- TOC entry 3329 (class 0 OID 17411)
-- Dependencies: 231
-- Data for Name: x_pe_referensi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_pe_referensi (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, name, "position", address_phone, relation) FROM stdin;
\.


--
-- TOC entry 3375 (class 0 OID 17641)
-- Dependencies: 277
-- Data for Name: x_placement; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_placement (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, client_id, employee_id, is_placement_active) FROM stdin;
\.


--
-- TOC entry 3311 (class 0 OID 17321)
-- Dependencies: 213
-- Data for Name: x_religion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_religion (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3349 (class 0 OID 17515)
-- Dependencies: 251
-- Data for Name: x_rencana_jadwal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_rencana_jadwal (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, schedule_code, schedule_date, "time", ro, tro, schedule_type_id, location, other_ro_tro, notes, is_automatic_mail, sent_date, status) FROM stdin;
\.


--
-- TOC entry 3351 (class 0 OID 17526)
-- Dependencies: 253
-- Data for Name: x_rencana_jadwal_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_rencana_jadwal_detail (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, rencana_jadwal_id, biodata_id) FROM stdin;
\.


--
-- TOC entry 3399 (class 0 OID 17751)
-- Dependencies: 301
-- Data for Name: x_resource_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_resource_project (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, client_id, location, department, pic_name, project_name, start_project, end_project, project_role, project_phase, project_description, project_technology, main_task) FROM stdin;
\.


--
-- TOC entry 3319 (class 0 OID 17356)
-- Dependencies: 221
-- Data for Name: x_riwayat_pekerjaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_riwayat_pekerjaan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, company_name, city, country, join_year, join_month, resign_year, resign_month, last_position, income, is_it_related, about_job, exit_reason, notes) FROM stdin;
\.


--
-- TOC entry 3325 (class 0 OID 17389)
-- Dependencies: 227
-- Data for Name: x_riwayat_pelatihan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_riwayat_pelatihan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, training_name, organizer, training_year, training_month, training_duration, time_period_id, city, country, notes) FROM stdin;
\.


--
-- TOC entry 3323 (class 0 OID 17378)
-- Dependencies: 225
-- Data for Name: x_riwayat_pendidikan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_riwayat_pendidikan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, school_name, city, country, education_level_id, entry_year, graduation_year, major, gpa, notes, orders, judul_ta, deskripsi_ta) FROM stdin;
\.


--
-- TOC entry 3321 (class 0 OID 17367)
-- Dependencies: 223
-- Data for Name: x_riwayat_proyek; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_riwayat_proyek (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, riwayat_pekerjaan_id, start_year, start_month, project_name, project_duration, time_period_id, client, project_position, description) FROM stdin;
\.


--
-- TOC entry 3301 (class 0 OID 17270)
-- Dependencies: 203
-- Data for Name: x_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_role (id, code, name, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
2	ROLE002	BOOTCAMP	1	2020-03-18	\N	\N	\N	\N	f
3	ROLE003	TRAINER	1	2020-03-18	\N	\N	\N	\N	f
1	ROLE001	INTERNAL SYSTEM DEVELOPMENT	1	2020-03-18	\N	\N	\N	\N	f
\.


--
-- TOC entry 3367 (class 0 OID 17609)
-- Dependencies: 269
-- Data for Name: x_schedule_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_schedule_type (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
1	1	2020-03-27	\N	\N	\N	\N	f	interview	\N
2	1	2020-03-27	\N	\N	\N	\N	f	test	\N
3	1	2020-03-27	\N	\N	\N	\N	f	seminar	\N
\.


--
-- TOC entry 3327 (class 0 OID 17400)
-- Dependencies: 229
-- Data for Name: x_sertifikasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_sertifikasi (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, certificate_name, publisher, valid_start_year, valid_start_month, until_year, until_month, notes) FROM stdin;
\.


--
-- TOC entry 3361 (class 0 OID 17585)
-- Dependencies: 263
-- Data for Name: x_skill_level; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_skill_level (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3333 (class 0 OID 17433)
-- Dependencies: 235
-- Data for Name: x_sumber_loker; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_sumber_loker (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, vacancy_source, candidate_type, event_name, career_center_name, referrer_name, referrer_phone, referrer_email, other_source, last_income, apply_date) FROM stdin;
\.


--
-- TOC entry 3365 (class 0 OID 17601)
-- Dependencies: 267
-- Data for Name: x_test_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_test_type (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3353 (class 0 OID 17553)
-- Dependencies: 255
-- Data for Name: x_time_period; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_time_period (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
\.


--
-- TOC entry 3393 (class 0 OID 17715)
-- Dependencies: 295
-- Data for Name: x_timesheet; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_timesheet (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, status, placement_id, timesheet_date, s_start, e_end, overtime, start_ot, end_ot, activity, user_approval, submitted_on, approved_on, ero_status, sent_on, collected_on) FROM stdin;
\.


--
-- TOC entry 3395 (class 0 OID 17723)
-- Dependencies: 297
-- Data for Name: x_timesheet_assessment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_timesheet_assessment (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, year, month, placement_id, target_result, competency, discipline) FROM stdin;
\.


--
-- TOC entry 3377 (class 0 OID 17649)
-- Dependencies: 279
-- Data for Name: x_training; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_training (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, code, name) FROM stdin;
\.


--
-- TOC entry 3379 (class 0 OID 17657)
-- Dependencies: 281
-- Data for Name: x_training_organizer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_training_organizer (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, notes) FROM stdin;
\.


--
-- TOC entry 3381 (class 0 OID 17665)
-- Dependencies: 283
-- Data for Name: x_training_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_training_type (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, code, name) FROM stdin;
\.


--
-- TOC entry 3402 (class 0 OID 17843)
-- Dependencies: 304
-- Data for Name: x_undangan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_undangan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, schedule_type_id, invitation_date, invitation_code, "time", ro, tro, other_ro_tro, location, status) FROM stdin;
2	1	2020-04-06	\N	\N	1	2020-04-06	t	2	2020-04-07	UD000002	15:00	2	1	romli	Bandung	\N
1	1	2020-04-06	\N	\N	1	2020-04-06	t	1	2020-04-08	UD000001	13:00	1	1	asddsad	Jakarta	\N
4	1	2020-04-06	\N	\N	\N	\N	f	2	2020-04-25	UD000004	14:30	1	2	debbyss	cipinang	\N
3	1	2020-04-06	\N	\N	\N	\N	f	2	2020-04-09	UD000003	14:30	2	1	Mba Debbys	balis	\N
5	1	2020-04-07	\N	\N	1	2020-04-09	t	3	2020-04-27	UD000005	13:40	1	2	Mas Imams	kalimantan	\N
6	1	2020-04-09	\N	\N	1	2020-04-09	t	2	2020-04-16	UD000006	15:40	2	1	mas imam	bali	\N
\.


--
-- TOC entry 3405 (class 0 OID 17853)
-- Dependencies: 307
-- Data for Name: x_undangan_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_undangan_detail (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, undangan_id, biodata_id, notes) FROM stdin;
2	1	2020-04-06	\N	\N	1	2020-04-06	t	2	4	ini user 2
1	1	2020-04-06	\N	\N	1	2020-04-06	t	1	3	ini user 1
4	1	2020-04-06	\N	\N	\N	\N	f	4	3	ini user 4
3	1	2020-04-06	\N	\N	\N	\N	f	3	4	ini und 3
5	1	2020-04-07	\N	\N	1	2020-04-09	t	5	3	Ini undangan 5
6	1	2020-04-09	\N	\N	1	2020-04-09	t	6	4	undangan ke 555
\.


--
-- TOC entry 3309 (class 0 OID 17313)
-- Dependencies: 211
-- Data for Name: x_userrole; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_userrole (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, addrbook_id, role_id) FROM stdin;
1	1	2020-03-19	\N	\N	\N	\N	f	1	1
2	1	2020-03-19	\N	\N	\N	\N	f	1	2
3	1	2020-03-24	\N	\N	\N	\N	f	2	2
\.


--
-- TOC entry 3465 (class 0 OID 0)
-- Dependencies: 308
-- Name: sequence_for_alpha_numeric; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sequence_for_alpha_numeric', 6, true);


--
-- TOC entry 3466 (class 0 OID 0)
-- Dependencies: 206
-- Name: x_addrbook_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_addrbook_id_seq', 2, true);


--
-- TOC entry 3467 (class 0 OID 0)
-- Dependencies: 218
-- Name: x_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_address_id_seq', 1, false);


--
-- TOC entry 3468 (class 0 OID 0)
-- Dependencies: 242
-- Name: x_biodata_attachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_biodata_attachment_id_seq', 1, false);


--
-- TOC entry 3469 (class 0 OID 0)
-- Dependencies: 208
-- Name: x_biodata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_biodata_id_seq', 4, true);


--
-- TOC entry 3470 (class 0 OID 0)
-- Dependencies: 244
-- Name: x_catatan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_catatan_id_seq', 1, false);


--
-- TOC entry 3471 (class 0 OID 0)
-- Dependencies: 284
-- Name: x_certification_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_certification_type_id_seq', 1, false);


--
-- TOC entry 3472 (class 0 OID 0)
-- Dependencies: 274
-- Name: x_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_client_id_seq', 1, false);


--
-- TOC entry 3473 (class 0 OID 0)
-- Dependencies: 270
-- Name: x_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_company_id_seq', 1, false);


--
-- TOC entry 3474 (class 0 OID 0)
-- Dependencies: 256
-- Name: x_education_level_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_education_level_id_seq', 1, false);


--
-- TOC entry 3475 (class 0 OID 0)
-- Dependencies: 272
-- Name: x_employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_employee_id_seq', 2, true);


--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 288
-- Name: x_employee_leave_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_employee_leave_id_seq', 1, false);


--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 286
-- Name: x_employee_training_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_employee_training_id_seq', 1, false);


--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 260
-- Name: x_family_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_family_relation_id_seq', 1, false);


--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 258
-- Name: x_family_tree_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_family_tree_type_id_seq', 1, false);


--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 214
-- Name: x_identity_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_identity_type_id_seq', 1, false);


--
-- TOC entry 3481 (class 0 OID 0)
-- Dependencies: 240
-- Name: x_keahlian_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_keahlian_id_seq', 1, false);


--
-- TOC entry 3482 (class 0 OID 0)
-- Dependencies: 238
-- Name: x_keluarga_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_keluarga_id_seq', 1, false);


--
-- TOC entry 3483 (class 0 OID 0)
-- Dependencies: 232
-- Name: x_keterangan_tambahan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_keterangan_tambahan_id_seq', 1, false);


--
-- TOC entry 3484 (class 0 OID 0)
-- Dependencies: 290
-- Name: x_leave_name_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_leave_name_id_seq', 1, false);


--
-- TOC entry 3485 (class 0 OID 0)
-- Dependencies: 292
-- Name: x_leave_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_leave_request_id_seq', 1, false);


--
-- TOC entry 3486 (class 0 OID 0)
-- Dependencies: 216
-- Name: x_marital_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_marital_status_id_seq', 1, false);


--
-- TOC entry 3487 (class 0 OID 0)
-- Dependencies: 204
-- Name: x_menu_access_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_menu_access_id_seq', 15, true);


--
-- TOC entry 3488 (class 0 OID 0)
-- Dependencies: 298
-- Name: x_menutree_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_menutree_id_seq', 9, true);


--
-- TOC entry 3489 (class 0 OID 0)
-- Dependencies: 264
-- Name: x_note_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_note_type_id_seq', 1, false);


--
-- TOC entry 3490 (class 0 OID 0)
-- Dependencies: 248
-- Name: x_online_test_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_online_test_detail_id_seq', 1, false);


--
-- TOC entry 3491 (class 0 OID 0)
-- Dependencies: 246
-- Name: x_online_test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_online_test_id_seq', 1, false);


--
-- TOC entry 3492 (class 0 OID 0)
-- Dependencies: 236
-- Name: x_organisasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_organisasi_id_seq', 1, false);


--
-- TOC entry 3493 (class 0 OID 0)
-- Dependencies: 230
-- Name: x_pe_referensi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_pe_referensi_id_seq', 1, false);


--
-- TOC entry 3494 (class 0 OID 0)
-- Dependencies: 276
-- Name: x_placement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_placement_id_seq', 1, false);


--
-- TOC entry 3495 (class 0 OID 0)
-- Dependencies: 212
-- Name: x_religion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_religion_id_seq', 1, false);


--
-- TOC entry 3496 (class 0 OID 0)
-- Dependencies: 252
-- Name: x_rencana_jadwal_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_rencana_jadwal_detail_id_seq', 1, false);


--
-- TOC entry 3497 (class 0 OID 0)
-- Dependencies: 250
-- Name: x_rencana_jadwal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_rencana_jadwal_id_seq', 1, false);


--
-- TOC entry 3498 (class 0 OID 0)
-- Dependencies: 300
-- Name: x_resource_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_resource_project_id_seq', 1, true);


--
-- TOC entry 3499 (class 0 OID 0)
-- Dependencies: 220
-- Name: x_riwayat_pekerjaan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_riwayat_pekerjaan_id_seq', 1, false);


--
-- TOC entry 3500 (class 0 OID 0)
-- Dependencies: 226
-- Name: x_riwayat_pelatihan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_riwayat_pelatihan_id_seq', 1, false);


--
-- TOC entry 3501 (class 0 OID 0)
-- Dependencies: 224
-- Name: x_riwayat_pendidikan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_riwayat_pendidikan_id_seq', 1, false);


--
-- TOC entry 3502 (class 0 OID 0)
-- Dependencies: 222
-- Name: x_riwayat_proyek_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_riwayat_proyek_id_seq', 1, false);


--
-- TOC entry 3503 (class 0 OID 0)
-- Dependencies: 202
-- Name: x_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_role_id_seq', 3, true);


--
-- TOC entry 3504 (class 0 OID 0)
-- Dependencies: 268
-- Name: x_schedule_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_schedule_type_id_seq', 3, true);


--
-- TOC entry 3505 (class 0 OID 0)
-- Dependencies: 228
-- Name: x_sertifikasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_sertifikasi_id_seq', 1, false);


--
-- TOC entry 3506 (class 0 OID 0)
-- Dependencies: 262
-- Name: x_skill_level_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_skill_level_id_seq', 1, false);


--
-- TOC entry 3507 (class 0 OID 0)
-- Dependencies: 234
-- Name: x_sumber_loker_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_sumber_loker_id_seq', 1, false);


--
-- TOC entry 3508 (class 0 OID 0)
-- Dependencies: 266
-- Name: x_test_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_test_type_id_seq', 1, false);


--
-- TOC entry 3509 (class 0 OID 0)
-- Dependencies: 254
-- Name: x_time_period_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_time_period_id_seq', 1, false);


--
-- TOC entry 3510 (class 0 OID 0)
-- Dependencies: 296
-- Name: x_timesheet_assessment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_timesheet_assessment_id_seq', 1, false);


--
-- TOC entry 3511 (class 0 OID 0)
-- Dependencies: 294
-- Name: x_timesheet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_timesheet_id_seq', 1, false);


--
-- TOC entry 3512 (class 0 OID 0)
-- Dependencies: 278
-- Name: x_training_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_training_id_seq', 1, false);


--
-- TOC entry 3513 (class 0 OID 0)
-- Dependencies: 280
-- Name: x_training_organizer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_training_organizer_id_seq', 1, false);


--
-- TOC entry 3514 (class 0 OID 0)
-- Dependencies: 282
-- Name: x_training_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_training_type_id_seq', 1, false);


--
-- TOC entry 3515 (class 0 OID 0)
-- Dependencies: 305
-- Name: x_undangan_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_undangan_detail_id_seq', 6, true);


--
-- TOC entry 3516 (class 0 OID 0)
-- Dependencies: 306
-- Name: x_undangan_detail_undangan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_undangan_detail_undangan_id_seq', 6, true);


--
-- TOC entry 3517 (class 0 OID 0)
-- Dependencies: 303
-- Name: x_undangan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_undangan_id_seq', 6, true);


--
-- TOC entry 3518 (class 0 OID 0)
-- Dependencies: 210
-- Name: x_userrole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_userrole_id_seq', 3, true);


--
-- TOC entry 3519 (class 0 OID 0)
-- Dependencies: 302
-- Name: xxx; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.xxx', 10000, true);


--
-- TOC entry 3079 (class 2606 OID 17299)
-- Name: x_addrbook x_addrbook_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_addrbook
    ADD CONSTRAINT x_addrbook_pkey PRIMARY KEY (id);


--
-- TOC entry 3091 (class 2606 OID 17353)
-- Name: x_address x_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_address
    ADD CONSTRAINT x_address_pkey PRIMARY KEY (id);


--
-- TOC entry 3115 (class 2606 OID 17485)
-- Name: x_biodata_attachment x_biodata_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_biodata_attachment
    ADD CONSTRAINT x_biodata_attachment_pkey PRIMARY KEY (id);


--
-- TOC entry 3081 (class 2606 OID 17310)
-- Name: x_biodata x_biodata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_biodata
    ADD CONSTRAINT x_biodata_pkey PRIMARY KEY (id);


--
-- TOC entry 3117 (class 2606 OID 17496)
-- Name: x_catatan x_catatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_catatan
    ADD CONSTRAINT x_catatan_pkey PRIMARY KEY (id);


--
-- TOC entry 3147 (class 2606 OID 17638)
-- Name: x_client x_client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_client
    ADD CONSTRAINT x_client_pkey PRIMARY KEY (id);


--
-- TOC entry 3143 (class 2606 OID 17622)
-- Name: x_company x_company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_company
    ADD CONSTRAINT x_company_pkey PRIMARY KEY (id);


--
-- TOC entry 3129 (class 2606 OID 17566)
-- Name: x_education_level x_education_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_education_level
    ADD CONSTRAINT x_education_level_pkey PRIMARY KEY (id);


--
-- TOC entry 3157 (class 2606 OID 17690)
-- Name: x_employee_leave x_employee_leave_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_employee_leave
    ADD CONSTRAINT x_employee_leave_pkey PRIMARY KEY (id);


--
-- TOC entry 3145 (class 2606 OID 17630)
-- Name: x_employee x_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_employee
    ADD CONSTRAINT x_employee_pkey PRIMARY KEY (id);


--
-- TOC entry 3155 (class 2606 OID 17682)
-- Name: x_employee_training x_employee_training_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_employee_training
    ADD CONSTRAINT x_employee_training_pkey PRIMARY KEY (id);


--
-- TOC entry 3133 (class 2606 OID 17582)
-- Name: x_family_relation x_family_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_family_relation
    ADD CONSTRAINT x_family_relation_pkey PRIMARY KEY (id);


--
-- TOC entry 3131 (class 2606 OID 17574)
-- Name: x_family_tree_type x_family_tree_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_family_tree_type
    ADD CONSTRAINT x_family_tree_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3087 (class 2606 OID 17334)
-- Name: x_identity_type x_identity_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_identity_type
    ADD CONSTRAINT x_identity_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3113 (class 2606 OID 17474)
-- Name: x_keahlian x_keahlian_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_keahlian
    ADD CONSTRAINT x_keahlian_pkey PRIMARY KEY (id);


--
-- TOC entry 3111 (class 2606 OID 17463)
-- Name: x_keluarga x_keluarga_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_keluarga
    ADD CONSTRAINT x_keluarga_pkey PRIMARY KEY (id);


--
-- TOC entry 3105 (class 2606 OID 17430)
-- Name: x_keterangan_tambahan x_keterangan_tambahan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_keterangan_tambahan
    ADD CONSTRAINT x_keterangan_tambahan_pkey PRIMARY KEY (id);


--
-- TOC entry 3159 (class 2606 OID 17701)
-- Name: x_leave_name x_leave_name_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_leave_name
    ADD CONSTRAINT x_leave_name_pkey PRIMARY KEY (id);


--
-- TOC entry 3161 (class 2606 OID 17712)
-- Name: x_leave_request x_leave_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_leave_request
    ADD CONSTRAINT x_leave_request_pkey PRIMARY KEY (id);


--
-- TOC entry 3089 (class 2606 OID 17342)
-- Name: x_marital_status x_marital_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_marital_status
    ADD CONSTRAINT x_marital_status_pkey PRIMARY KEY (id);


--
-- TOC entry 3077 (class 2606 OID 17291)
-- Name: x_menu_access x_menu_access_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_menu_access
    ADD CONSTRAINT x_menu_access_pkey PRIMARY KEY (id);


--
-- TOC entry 3167 (class 2606 OID 17740)
-- Name: x_menutree x_menutree_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_menutree
    ADD CONSTRAINT x_menutree_pkey PRIMARY KEY (id);


--
-- TOC entry 3137 (class 2606 OID 17598)
-- Name: x_note_type x_note_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_note_type
    ADD CONSTRAINT x_note_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3121 (class 2606 OID 17512)
-- Name: x_online_test_detail x_online_test_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_online_test_detail
    ADD CONSTRAINT x_online_test_detail_pkey PRIMARY KEY (id);


--
-- TOC entry 3119 (class 2606 OID 17504)
-- Name: x_online_test x_online_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_online_test
    ADD CONSTRAINT x_online_test_pkey PRIMARY KEY (id);


--
-- TOC entry 3109 (class 2606 OID 17452)
-- Name: x_organisasi x_organisasi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_organisasi
    ADD CONSTRAINT x_organisasi_pkey PRIMARY KEY (id);


--
-- TOC entry 3103 (class 2606 OID 17419)
-- Name: x_pe_referensi x_pe_referensi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_pe_referensi
    ADD CONSTRAINT x_pe_referensi_pkey PRIMARY KEY (id);


--
-- TOC entry 3149 (class 2606 OID 17646)
-- Name: x_placement x_placement_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_placement
    ADD CONSTRAINT x_placement_pkey PRIMARY KEY (id);


--
-- TOC entry 3085 (class 2606 OID 17326)
-- Name: x_religion x_religion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_religion
    ADD CONSTRAINT x_religion_pkey PRIMARY KEY (id);


--
-- TOC entry 3125 (class 2606 OID 17531)
-- Name: x_rencana_jadwal_detail x_rencana_jadwal_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_rencana_jadwal_detail
    ADD CONSTRAINT x_rencana_jadwal_detail_pkey PRIMARY KEY (id);


--
-- TOC entry 3123 (class 2606 OID 17523)
-- Name: x_rencana_jadwal x_rencana_jadwal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_rencana_jadwal
    ADD CONSTRAINT x_rencana_jadwal_pkey PRIMARY KEY (id);


--
-- TOC entry 3169 (class 2606 OID 17759)
-- Name: x_resource_project x_resource_project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_resource_project
    ADD CONSTRAINT x_resource_project_pkey PRIMARY KEY (id);


--
-- TOC entry 3093 (class 2606 OID 17364)
-- Name: x_riwayat_pekerjaan x_riwayat_pekerjaan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_pekerjaan
    ADD CONSTRAINT x_riwayat_pekerjaan_pkey PRIMARY KEY (id);


--
-- TOC entry 3099 (class 2606 OID 17397)
-- Name: x_riwayat_pelatihan x_riwayat_pelatihan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_pelatihan
    ADD CONSTRAINT x_riwayat_pelatihan_pkey PRIMARY KEY (id);


--
-- TOC entry 3097 (class 2606 OID 17386)
-- Name: x_riwayat_pendidikan x_riwayat_pendidikan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_pendidikan
    ADD CONSTRAINT x_riwayat_pendidikan_pkey PRIMARY KEY (id);


--
-- TOC entry 3095 (class 2606 OID 17375)
-- Name: x_riwayat_proyek x_riwayat_proyek_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_riwayat_proyek
    ADD CONSTRAINT x_riwayat_proyek_pkey PRIMARY KEY (id);


--
-- TOC entry 3075 (class 2606 OID 17275)
-- Name: x_role x_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_role
    ADD CONSTRAINT x_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3141 (class 2606 OID 17614)
-- Name: x_schedule_type x_schedule_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_schedule_type
    ADD CONSTRAINT x_schedule_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3101 (class 2606 OID 17408)
-- Name: x_sertifikasi x_sertifikasi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_sertifikasi
    ADD CONSTRAINT x_sertifikasi_pkey PRIMARY KEY (id);


--
-- TOC entry 3135 (class 2606 OID 17590)
-- Name: x_skill_level x_skill_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_skill_level
    ADD CONSTRAINT x_skill_level_pkey PRIMARY KEY (id);


--
-- TOC entry 3107 (class 2606 OID 17441)
-- Name: x_sumber_loker x_sumber_loker_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_sumber_loker
    ADD CONSTRAINT x_sumber_loker_pkey PRIMARY KEY (id);


--
-- TOC entry 3139 (class 2606 OID 17606)
-- Name: x_test_type x_test_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_test_type
    ADD CONSTRAINT x_test_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3127 (class 2606 OID 17558)
-- Name: x_time_period x_time_period_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_time_period
    ADD CONSTRAINT x_time_period_pkey PRIMARY KEY (id);


--
-- TOC entry 3165 (class 2606 OID 17728)
-- Name: x_timesheet_assessment x_timesheet_assessment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_timesheet_assessment
    ADD CONSTRAINT x_timesheet_assessment_pkey PRIMARY KEY (id);


--
-- TOC entry 3163 (class 2606 OID 17720)
-- Name: x_timesheet x_timesheet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_timesheet
    ADD CONSTRAINT x_timesheet_pkey PRIMARY KEY (id);


--
-- TOC entry 3153 (class 2606 OID 17662)
-- Name: x_training_organizer x_training_organizer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training_organizer
    ADD CONSTRAINT x_training_organizer_pkey PRIMARY KEY (id);


--
-- TOC entry 3151 (class 2606 OID 17654)
-- Name: x_training x_training_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training
    ADD CONSTRAINT x_training_pkey PRIMARY KEY (id);


--
-- TOC entry 3173 (class 2606 OID 17862)
-- Name: x_undangan_detail x_undangan_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_undangan_detail
    ADD CONSTRAINT x_undangan_detail_pkey PRIMARY KEY (id);


--
-- TOC entry 3171 (class 2606 OID 17848)
-- Name: x_undangan x_undangan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_undangan
    ADD CONSTRAINT x_undangan_pkey PRIMARY KEY (id);


--
-- TOC entry 3083 (class 2606 OID 17318)
-- Name: x_userrole x_userrole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_userrole
    ADD CONSTRAINT x_userrole_pkey PRIMARY KEY (id);


-- Completed on 2020-04-10 18:40:20

--
-- PostgreSQL database dump complete
--

