const ResponseHelper = require("../Helpers/responseHelper");
const dtl_user = require("../DataLayer/Dtl_User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../Config/authconfig.json");
const L_User = {
  readRoleAllHandler: (req, res, next) => {
    console.log(req.body);
    var docs = req.body;
    dtl_user.readRoleAllHandlerData(function(items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  readMenuAllHandler: (req, res, next) => {
    console.log(req.body);
    var docs = req.body;
    dtl_user.readMenuAllHandlerData(function(items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  editPasswordAllHandler: (req, res, next) => {
    console.log(req.body);
    var docs = req.body;
    dtl_user.editPasswordAllHandlerData(function(items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  createUserAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_user.createUserAllHandlerData(function(items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  login: (req, res, next) => {
    let data = req.body;
    dtl_user.readOneUserByIdData(function(items) {
      if (items[0]) {
        if (items[0].is_locked === true) {
          let result = "coba lagi nanti";
          ResponseHelper.sendResponse(res, 404, result);
        } else {
          console.log(JSON.stringify(items));
          console.log(data);

          if (bcrypt.compareSync(data.password, items[0].abpwd)) {
            let token = jwt.sign(items[0], authConfig.secretkey);
            let result = {
              userdata: items[0],
              token: token
            };
            let usern = items[0].abuid;
            let attempt = items[0].attempt + 1;
            data = {
              usern: usern,
              attempt: attempt
            };
            dtl_user.ulangUserAllHandler3(data);
            ResponseHelper.sendResponse(res, 200, result);
          } else {
            let count = items[0].fp_counter + 1;
            let attempt = items[0].attempt + 1;
            let usern = items[0].abuid;
            data = {
              count: count,
              attempt: attempt,
              usern: usern
            };
            dtl_user.ulangUserAllHandler1(data);
            if (count <= 2) {
              let result = "wrong password";
              ResponseHelper.sendResponse(res, 404, result);
            } else if (count > 2) {
              dtl_user.ulangUserAllHandler2(data);
              let result = "password salah lebih dari tiga kali";
              ResponseHelper.sendResponse(res, 404, result);
            }
            console.log(count);
          }
        }
      } else {
        let result = "user not found";
        ResponseHelper.sendResponse(res, 404, result);
      }
    }, data.username);
  }
};
module.exports = L_User;
