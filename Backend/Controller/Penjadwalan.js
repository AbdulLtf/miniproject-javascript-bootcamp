const ResponseHelper = require("../Helpers/responseHelper");
const dtl_penjadwalan = require("../DataLayer/dtl_penjadwalan");
const Penjadwalan = {
  readUndanganAscAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.readUndanganAscAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  readUndanganDescAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.readUndanganDescAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  // backup get undangan
  // readUndanganAllHandler: (req, res, next) => {
  //   dtl_penjadwalan.readUndanganAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   });
  // },
  readRoTroAllHandler: (req, res, next) => {
    dtl_penjadwalan.readRoTroAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  readJenisUndanganAllHandler: (req, res, next) => {
    dtl_penjadwalan.readJenisUndanganAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  readPelamarAllHandler: (req, res, next) => {
    dtl_penjadwalan.readPelamarAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  createUndanganAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.createUndanganAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  deleteUndanganAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.deleteUndanganAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  deleteUndanganDetailAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.deleteUndanganDetailAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  editUndanganAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.editUndanganAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  editUndanganDetailAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.editUndanganDetailAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  readPostRoTroAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.readPostRoTroAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  readListRencanaAllHandler: (req, res, next) => {
    dtl_penjadwalan.readListRencanaAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  getListRencanaAscAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.getListRencanaAscAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  getListRencanaDescAllHandler: (req, res, next) => {
    var docs = req.body;
    dtl_penjadwalan.getListRencanaDescAllHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  // readResourceAllHandler: (req, res, next) => {
  //   dtl.readResourceAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   });
  // },
  // readResourceDescAllHandler: (req, res, next) => {
  //   dtl.readResourceDescAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   });
  // },
  // readResourceAscAllHandler: (req, res, next) => {
  //   dtl.readResourceAscAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   });
  // },
  // hapusListAllHandler: (req, res, next) => {
  //   var docs = req.body;

  //   dtl.hapusListAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   }, docs);
  // },
  // readClientAllHandler: (req, res, next) => {
  //   dtl.readClientAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   });
  // },
  // updateClientAllHandler: (req, res, next) => {
  //   var docs = req.body;
  //   dtl.updateClientAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   }, docs);
  // },
  // tambahClientAllHandler: (req, res, next) => {
  //   var docs = req.body;
  //   console.log(req.body);
  //   dtl.tambahClientAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   }, docs);
  // },
  // readResourceFindAllHandler: (req, res, next) => {
  //   var docs = req.body;
  //   console.log(req.body);
  //   dtl.readResourceFindAllHandlerData(function (items) {
  //     ResponseHelper.sendResponse(res, 200, items);
  //   }, docs);
  // },
};
module.exports = Penjadwalan;
