const UserLogic = require("../Controller/User");
const PenjadwalanLogic = require("../Controller/Penjadwalan");

module.exports = exports = function (server) {
  server.post("/api/createuser", UserLogic.createUserAllHandler);
  server.put("/api/ubahpassword", UserLogic.editPasswordAllHandler);
  server.post("/api/login", UserLogic.login);
  server.post("/api/userrole", UserLogic.readRoleAllHandler);
  server.post("/api/usermenu", UserLogic.readMenuAllHandler);
  // backup getundangan
  // server.get("/api/getundangan", PenjadwalanLogic.readUndanganAllHandler);
  server.post(
    "/api/getundanganasc",
    PenjadwalanLogic.readUndanganAscAllHandler
  );
  server.post(
    "/api/getundangandesc",
    PenjadwalanLogic.readUndanganDescAllHandler
  );
  server.get("/api/getrotro", PenjadwalanLogic.readRoTroAllHandler);
  server.get("/api/getjenun", PenjadwalanLogic.readJenisUndanganAllHandler);
  server.get("/api/pelamar", PenjadwalanLogic.readPelamarAllHandler);
  server.post("/api/createundangan", PenjadwalanLogic.createUndanganAllHandler);
  server.put("/api/deleteundangan", PenjadwalanLogic.deleteUndanganAllHandler);
  server.put(
    "/api/deleteundangandetail",
    PenjadwalanLogic.deleteUndanganDetailAllHandler
  );
  server.put("/api/editundangan", PenjadwalanLogic.editUndanganAllHandler);
  server.put(
    "/api/editundangandetail",
    PenjadwalanLogic.editUndanganDetailAllHandler
  );
  server.post("/api/namarotro", PenjadwalanLogic.readPostRoTroAllHandler);
  server.get("/api/listrencana", PenjadwalanLogic.readListRencanaAllHandler);
  server.post(
    "/api/listrencanaasc",
    PenjadwalanLogic.getListRencanaAscAllHandler
  );
  server.post(
    "/api/listrencanadesc",
    PenjadwalanLogic.getListRencanaDescAllHandler
  );
};
